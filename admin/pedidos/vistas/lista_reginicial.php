<?php
@session_start();
//MUESTRO ALGUNA INFORMACION SI Y SOLO SI EL USUARIO SE ENCUENTRA LOGUEADO EN EL SISTEMA
if((isset($_SESSION['id_usuario'])) && ($_SESSION['id_usuario']!="")) {
	if (file_exists('conf.php')){
		include('conf.php');
	}else{
		include('plantillas/conf.php');
	}
	
	include_once (DIR_WS_CLASES."Pedidos.php");
	$pedidos =  new Pedidos();
	$estatus = 1;
	//SE CONSULTAN LOS REGISTROS CON ESTATUS 1 ES DECIR LOS NO REVISADOS
	$resultado = $pedidos->consultarRegistroInicial($estatus);
	
	?>
	<input type="hidden" name="idregistroinicialinput" id="idregistroinicialinput" value="">
	<div id="listaregistroinicial" style="width:675px; height:300px; overflow-y: auto; overflow-x: hidden; padding: 5px; float: left;">
	    <table id="tblRegistroInicial"  class="tabla">
	    	<tr><th colspan="5">Registro Inicial</th></tr>
	    	<tr>
	    		<th>Tel&eacute;fono</th>
	    		<th>Email</th>
	    		<th>Codigo</th>
	    		<th>Fecha</th>
	    		<td class="botonAdmin">
	    			<input type="submit" class="actualizar-registro-inicial" value="Actualizar">
	    		</td>
	    	</tr>
		<?php
		foreach ($resultado as $value){
			$date = date_create($value['fecha_hora']);
		?>
			<tr class="modo1" onclick="cambiarColorCelda(this)">
				<td class="modo2"><input type="hidden" id="telefono_<?php echo $value['id_registro_inicial']; ?>" value="<?php echo $value['tel_cliente']; ?>"></input><?php echo $value['tel_cliente']; ?></td>
				<td class="modo2"><?php echo $value['email_cliente']; ?></td>
				<td class="modo2"><?php echo $value['codigo_cliente']; ?></td>
				<td class="modo2"><?php echo date_format($date, 'd-m-Y H:i'); ?></td>
				<td class="modo2"><input type="hidden" id="email_<?php echo $value['id_registro_inicial']; ?>" value="<?php echo $value['email_cliente']; ?>"></input><a id="<?php echo $value['id_registro_inicial']; ?>" class="ver-pedido" >Ver pedido</a></td>
			</tr>
		<?php 
		}		
		?>
		</table>
	</div>
	<script type='text/javascript' src="../../../js/ajax.js"></script>
<?php 
}else {
?>
	<div id="listaregistroinicial"></div>
<?php 	
}
?>
