<?php
@session_start();
//MUESTRO ALGUNA INFORMACION SI Y SOLO SI EL USUARIO SE ENCUENTRA LOGUEADO EN EL SISTEMA
if((isset($_SESSION['id_usuario'])) && ($_SESSION['id_usuario']!="")) {
	if (file_exists('conf.php')){
		include('conf.php');
	}else{
		include('plantillas/conf.php');
	}
	
	include_once (DIR_WS_CLASES."Clientes.php");
	include_once (DIR_WS_CLASES."Funciones.php");
	include_once (DIR_WS_CLASES."Funciones.php");
	
	$conectado = new Conexion();
	
	$conectado->abrir_conexion();
	//LA FUNCION PRINCIPAL DE LA FUNCION LIMPIAR VARIABLE CONSISTE EN UTILIZAR LA FUNCION mysql_real_escape_string PARA EVITAR INJECCION SQL
	//CAPTURO LOS DATOS DEL FORMULARIO/
	$nombrecliente = limpiar_variable($_POST['nombrecliente']);
	$nit           = limpiar_variable($_POST['nit']);
	$telefono      = limpiar_variable($_POST['telefono']);
	$direccion     = limpiar_variable($_POST['direccion']);
	$ciudad        = limpiar_variable($_POST['ciudad']);
	$municipio     = limpiar_variable($_POST['municipio']);
	$estado        = limpiar_variable($_POST['estado']);
	$pais          = limpiar_variable($_POST['pais']);
	$tipopersona   = limpiar_variable($_POST['tipopersona']);
	$email         = limpiar_variable($_POST['email']);
	$codigo        = limpiar_variable($_POST['codigo']);
	$tipooperacion = limpiar_variable($_POST['tipooperacion']);
	$emailoriginal = limpiar_variable($_POST['emailoriginal']);
	$emailget      = limpiar_variable($_GET['email']);
	$conectado->cerrar_conexion();
	
	if(!isset($email) || $email==""){
		if(!isset($emailget) || $emailget==""){
			$email = "";
		}else {
			//SE COLOCA LA VARIABLE DE EMAIL QUE VIENE POR GET Y SE LE ASIGNA A LA VARIABLE EMAIL
			$email = $emailget;
		}
	}
	
	$clientes =  new Clientes();
	$resultado = $clientes->consultarClientes($email,$nombrecliente,$nit,$telefono,$direccion,$ciudad,$municipio,$estado,$pais,$tipopersona,$codigo);
	
	?>
	<div id="datoscliente" style="width:672px; height:300px; overflow-y: auto; overflow-x: hidden; padding: 5px; float: left;">
		<form id="guardarDatosCliente">
		<?php 
		if($resultado){ 
		?>
			<input type="hidden" id="idcliente" name="idcliente" value="<?php echo $resultado[0]['id_cliente'];  ?>" >
			<input type="hidden" id="tipooperacion" name="tipooperacion" value="2" >
			<input type="hidden" id="emailoriginal" name="emailoriginal" value="<?php echo $resultado[0]['email_cliente'];  ?>" >
			
		    <table class="tabla" id="tableDatosClientes">
		    	<tr>
		    		<th colspan="3">Datos del Cliente</th>
		    		<td class="botonAdmin"><input type="submit" class="cerrar-session" value="Cerra Session"></td>
		    	</tr>
		    	<tr class="modo1">
		    		<td>Nombre: </td><td><input type="text" id="nombrecliente" name="nombrecliente" value="<?php echo $resultado[0]['nom_cliente'];  ?>" ></td>
		    		<td>NIT: </td><td><input type="text" id="nit" value="<?php echo $resultado[0]['nit_cliente'];  ?>" ></td>
		    	</tr>	
		    	<tr class="modo1">	
		    		<td class="campoResaltado">Tel&eacute;fono: </td><td class="campoResaltado"><input type="text" id="telefono" name="telefono"  value="<?php echo $resultado[0]['tel_cliente'];  ?>" ></td>
		    		<td>Direccion: </td><td><input type="text" id="direccion"      name="direccion" value="<?php echo $resultado[0]['dir_cliente'];  ?>" ></td>
		    	</tr>
		    	<tr class="modo1">	
		    		<td>Ciudad: </td><td><input type="text" id="ciudad"       name="ciudad" value="<?php echo $resultado[0]['ciu_cliente'];  ?>" ></td>
		    		<td>Municipio: </td><td><input type="text" id="municipio" name="municipio" value="<?php echo $resultado[0]['mun_cliente'];  ?>"></td>
		    	</tr>
		    	<tr class="modo1">	
		    		<td>Estado: </td><td><input type="text" id="estado" name="estado" value="<?php echo $resultado[0]['est_cliente'];  ?>"></td>
		    		<td>Pais: </td><td><input type="text" id="pais"     name="pais" value="<?php echo $resultado[0]['pais_cliente'];  ?>"></td>
		    	</tr>
		    	<tr class="modo1">	
		    		<td class="campoResaltado">Email: </td><td class="campoResaltado"><input type="text" id="email"   name="email" value="<?php echo $resultado[0]['email_cliente'];  ?>"></td>
		    		<td class="campoResaltado">Codigo: </td><td class="campoResaltado"><input type="text" id="codigo" name="codigo" value="<?php echo $resultado[0]['codigo_cliente'];  ?>"></td>
		    	</tr>
		    	<tr class="modo1">	
		    		<td>Tipo de Cliente: </td>
		    		<td>
		    			<select id="tipopersona" name="tipopersona">
		    				<?php if($resultado[0]['tip_cliente'] == 1) {?>
			    				<option value="1" selected>Natural</option>
			    				<option value="2">Jur&iacute;dico</option>
		    				<?php } else if($resultado[0]['tip_cliente'] == 2) {  ?>
			    				<option value="1">Natural</option>
			    				<option value="2" selected>Jur&iacute;dico</option>
			    			<?php } ?>
		    			</select>
		    		</td>
		    		<td colspan="2">&nbsp;</td>
		    	</tr>
		    	<tr class="modo1">	
		    		<td colspan="4">
		    			<center>
		    			<input type="submit" value="Actualizar" class="guardar">
		    			&nbsp;<input type="button" value="Limpiar" id="limpiar" class="limpiar">
		    			&nbsp;<input onclick="resetearColorCeldas(), actualizarTipoOperacion(3)" type="button" value="Buscar"    name="buscarcliente" class="buscarcliente" id="buscarcliente">
		    			&nbsp;<input onclick="resetearColorCeldas(), actualizarTipoOperacion(4)" type="button" value="Historico" name="buscarcliente" class="buscarcliente" id="buscarcliente">
		    			</center>
		    		</td>
		    	</tr>
			</table>
		<?php 
		} else { //EN EL CASO QUE EL CLIENTE NO HAYA SIDO ENCONTRADO
		?>
			<input type="hidden" id="idcliente" name="idcliente" value="" >
			<input type="hidden" id="tipooperacion" name="tipooperacion" value="1" >
			<input type="hidden" id="emailoriginal" name="emailoriginal" value="" >
				
		    <table class="tabla" id="tableDatosClientes">
		    	<tr>
		    		<th colspan="3">Datos del Cliente</th>
		    		<td class="botonAdmin"><input type="submit" class="cerrar-session" value="Cerra Session"></td>
		    	</tr>
		    	<tr class="modo1">
		    		<td>Nombre: </td><td><input type="text" id="nombrecliente" name="nombrecliente" value="" ></td>
		    		<td>NIT: </td><td><input type="text" id="nit" name="nit" value="" ></td>
		    	</tr>	
		    	<tr class="modo1">	
		    		<td class="campoResaltado">Tel&eacute;fono: </td><td class="campoResaltado"><input type="text"  id="telefono" name="telefono" value="<?php echo $telefono; ?>" ></td>
		    		<td>Direccion: </td><td><input type="text" id="direccion" name="direccion" value="" ></td>
		    	</tr>
		    	<tr class="modo1">	
		    		<td>Ciudad: </td><td><input type="text" id="ciudad" name="ciudad" value="" ></td>
		    		<td>Municipio: </td><td><input type="text" id="municipio" name="municipio" value=""></td>
		    	</tr>
		    	<tr class="modo1">	
		    		<td>Estado: </td><td><input type="text" id="estado" name="estado" value=""></td>
		    		<td>Pais: </td><td><input type="text" id="pais" name="pais" value=""></td>
		    	</tr>
		    	<tr class="modo1">	
		    		<td class="campoResaltado">Email: </td><td class="campoResaltado"><input type="text" id="email" name="email"    value="<?php echo $email; ?>" ></td>
		    		<td class="campoResaltado">Codigo: </td><td class="campoResaltado"><input type="text" id="codigo" name="codigo" value=""></td>
		    	</tr>
		    	<tr class="modo1">	
		    		<td>Tipo de Cliente: </td><td><select id="tipopersona" name="tipopersona"><option value="1">Natural</option><option value="2">Jur&iacute;dico</option></select></td>
		    		<td colspan="2">&nbsp;</td>
		    	</tr>
		    	<tr class="modo1">	
		    		<td colspan="4">
		    			<center>
			    			<input type="submit" value="Guardar" class="guardar">
			    			&nbsp;<input type="button" value="Limpiar" id="limpiar" class="limpiar">
			    			&nbsp;<input onclick="resetearColorCeldas(), actualizarTipoOperacion(3)" type="button" value="Buscar" name="buscarcliente" class="buscarcliente" id="buscarcliente">
			    			&nbsp;<input onclick="resetearColorCeldas(), actualizarTipoOperacion(4)" type="button" value="Historico" name="buscarcliente" class="buscarcliente" id="buscarcliente">
		    		 	</center>
		    		 </td>	
		    	</tr>
			</table>
		<?php 
		}
		?>	
		</form>	
	</div>
	<script type='text/javascript' src="../../../js/jquery.validate.js"></script>
	<script type='text/javascript' src="../../../js/ajax.js"></script>
<?php 
} else {
?>
	<div id="datoscliente"></div>
<?php 
}
?>