<?php
@session_start();
//MUESTRO ALGUNA INFORMACION SI Y SOLO SI EL USUARIO SE ENCUENTRA LOGUEADO EN EL SISTEMA
if((isset($_SESSION['id_usuario'])) && ($_SESSION['id_usuario']!="")) {
	if (file_exists('conf.php')){
		include('conf.php');
	}else{
		include('plantillas/conf.php');
	}
	
	include_once (DIR_WS_CLASES."Pedidos.php");
	
	//SE FILTRAN LOS PEDIDOS POR EL ID DEL REGISTRO INICIAL QUE VIENE POR PARAMETROS
	$idregistroinicial = $_GET['idregistroinicial'];
	if(!isset($idregistroinicial)){
		$idregistroinicial = 0;
	}
	
	$pedidos =  new Pedidos();
	$resultado      = $pedidos->consultarPedidosIniciales($idregistroinicial);
	$resultadoTotal = $pedidos->consultarPedidosInicialesTotal($idregistroinicial);
	
	?>
	<script>
		$(function () {
		$(".datepicker").datepicker();
		});
	</script>
	<div id="listapedidosinicial" style="width:672px; height:300px; overflow-y: auto; overflow-x: hidden; padding: 5px; float: left;">
		<form id="generarpedido" method="post">
			    <table  class="tabla" id="tblDatosProductoInicial">
			    	<tr><th colspan="4">Pedido Inicial</th></tr>
			    	<tr >
			    		<th>Nombre Producto</th>
			    		<th>Cantidad</th>
			    		<th>Fecha Registro</th>
			    		<th>Fecha de Entrega</th>
			    	</tr>
				<?php
				$i=1;

				foreach ($resultado as $value){
					$date = date_create($value['fecha_pedido']);
				?>
					<tr class="modo1" id="<?php echo $value['id_producto']; ?>">
						<td class="modo2"><?php echo utf8_decode($value['nom_producto']); ?></td>
						<td class="modo2">
							<div class="cantidadInput">
								<input class="actualizar-cantidad-escrita" onKeypress="return validarNumero(event);" type="text" id="cantidad_<?php echo $value['id_producto']; ?>" maxlength="4"  type="text" value="<?php echo $value['cant_producto']; ?>">
								<input type="hidden" id="cantidad_original_<?php echo $value['id_producto']; ?>" value="<?php echo $value['cant_producto']; ?>">
							</div>
							<div class="incrementaDecrementa">
								<a class="actualizar-cantidad" id="<?php echo $value['id_producto']; ?>" onclick="aumentarCantidad('<?php echo $value['id_producto']; ?>')"><img src="../../../imagenes/incrementa.png"></a>
				            <br>
								<a class="actualizar-cantidad" id="<?php echo $value['id_producto']; ?>" onclick="disminuirCantidad('<?php echo $value['id_producto']; ?>')"><img src="../../../imagenes/decrementa.png"></a>
							</div>
							<div class="unidades"> 
		    					<?php echo $value['unidad_tipo'];?>
		    					<input type="hidden"  id="id_tipo_<?php echo $value['id_producto']; ?>" name="id_tipo_<?php echo $value['id_producto']; ?>" value="<?php echo $value['id_tipos']; ?>"  />
							</div>
						</td>
						<td class="modo2"><?php echo date_format($date, 'd-m-Y H:i'); ?></td>
						<!--  SOLO MUESTRO EL IMPUT DE LA FECHA UNA SOLA VEZ. --> 
						<?php if ($i == 1 ) { ?>
						<td rowspan="0" class="modo3">
							<input readonly type="text" class="datepicker" id="fechaentrega" name="fechaentrega" >
						</td>
						<?php } ?>
						
					</tr>
				<?php 
				$i++;
				}	
				?>
				</table>
				<br>
				
				
				<table  class="tabla">
			    	<tr><th colspan="5">Totales</th></tr>
			    	<tr>
			    		<th>Tipo de Producto</th>
			    		<th>Cantidad</th>
			    		<th>Precio M&iacute;nimo</th>
			    		<th>Precio M&aacute;ximo</th>
			    		<th>Precio pedido</th>
			    	</tr>
			     	
				<?php foreach ($resultadoTotal as $value){ ?>  
					<tr class="modo1">
						<td class="modo2"><?php echo $value['unidad_tipo']; ?></td>
						<td class="modo2">
							<div id="cantidad_total_<?php echo $value['id_tipos']; ?>" ><?php echo $value['cant_producto']; ?></div>
						</td>
						<td class="modo2">
							<div id="minprecio_<?php echo $value['id_tipos']; ?>"><?php echo number_format($value['minprecio'], 2, ",", ".");  ?></div>
							<input type="hidden" id="minprecio_value_<?php echo $value['id_tipos']; ?>" value="<?php echo number_format($value['minprecio'], 2, ",", "."); ?>">
						</td>
						<td class="modo2">
							<div id="maxprecio_<?php echo $value['id_tipos']; ?>"><?php echo  number_format($value['maxprecio'], 2, ",", ".");  ?></div>
							<input type="hidden" id="maxprecio_value_<?php echo $value['id_tipos']; ?>" value="<?php echo  number_format($value['maxprecio'], 2, ",", "."); ?>">
						</td>
						<td class="modo3">
							<input name="precio_pedido_<?php echo $value['id_tipos']; ?>" maxlength="10" class="decimal" type="text"  id="precio_pedido_<?php echo $value['id_tipos']; ?>" name="precio_pedido_<?php echo $value['id_tipos']; ?>" value="" >
						</td>
					</tr>
				<?php } ?>
				</table>
				
				<br>

				<!-- HAY REGISTROS DE PEDIDOS INICIALES -->
				<?php if($i> 1) { ?>
					<input type="button" class="eliminar-pedido" value="Eliminar Pedido">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="submit" class="btn-generar-pedido" value="Generar Pedido">
				<?php } else { ?>
					<h2>No existen pedidos disponibles</h2>
				<?php } ?>
		</form>	
	</div>
	<script type='text/javascript' src="../../../js/jquery.validate.js"></script>
	<script type='text/javascript' src="../../../js/ajax.js"></script>
	<script type='text/javascript' src="../../../js/jquery.numeric.js"></script>
<?php } else { ?>
	<div id="listapedidosinicial"></div>
<?php 
}?>