<?php
@session_start();
//MUESTRO ALGUNA INFORMACION SI Y SOLO SI EL USUARIO SE ENCUENTRA LOGUEADO EN EL SISTEMA
if((isset($_SESSION['id_usuario'])) && ($_SESSION['id_usuario']!="")) {
	if (file_exists('conf.php')){
		include('conf.php');
	}else{
		include('plantillas/conf.php');
	}
	
	include_once (DIR_WS_CLASES."Pedidos.php");
	$pedidos =  new Pedidos();
	$idcliente     = $_GET['idcliente'];
	$tipooperacion = $_GET['tipooperacion'];
	//TIPO OPERACION = 3 BUSQUEDA DE PEDIDOS , TIPO OPERACION = 4 BUSQUEDA DE PEDIDOS HISTORICOS
	
	if(!isset($idcliente) || $idcliente== null || $idcliente==""){
		$idcliente = 0;
	}
	
	if($tipooperacion == 3){
		$estatus = 1;
		//SE CONSULTA LOS REGISTROS CON ESTATUS 1 QUE SINGIFICA NO ENTREGADO
	}else {
		//SE CONSULTAN TODOS LOS REGISTROS
		$estatus="";
	}
	
	
	$resultado = $pedidos->consultarPedidosFinal($idcliente,$estatus);
	?>
	<div id="listapedidosfinal" style="width:672px; height:300px; overflow-y: auto; overflow-x: hidden; padding: 5px; float: left;">
	    <table  class="tabla">
	    	<tr>
	    		<th>Id Pedido</th>
	    		<th>Producto</th>
	    		<th>Cantidad</th>
	    		<th>Precio</th>
	    		<th>Fecha Pedido</th>
	    		<th>Fecha Entrega</th>
	    		<th>Entregado?</th>
	    	</tr>
		<?php
		$i=1;
		foreach ($resultado as $value){
			$date        = date_create($value['fecha_pedido']);
			$dateentrega = date_create($value['fecha_entrega_pedido']);
			
			//PARA LA PRIMERA VEZ QUE ENTRA NO PINTA NINGUN TOTAL
			if($i != 1){
				//LOGICA PARA ESCRIBIR EL TOTAL PARA CADA PEDIDO 
			 	if($idPedidoOriginal != $value['id_pedido']) {
			 		?>
			 			<tr class="modo1"><td class="modo2">Total : <?php echo number_format($montoTotalPedido, 2, ",", ".");  ?></td></tr>
			 		<?php 
			 	}	
			}
		?>
			<tr class="modo1">
				<td class="modo2"><?php echo $value['id_pedido']; ?></td>
				<td class="modo2"><?php echo utf8_decode($value['nom_producto']); ?></td>
				<td class="modo2"><?php echo $value['cant_producto']; ?></td>
				<td class="modo2"><?php echo number_format($value['pvp_producto'], 2, ",", ".");  ?></td>
				<td class="modo2"><?php echo date_format($date, 'd-m-Y H:i'); ?></td>
				<td class="modo2"><?php echo date_format($dateentrega, 'd-m-Y'); ?></td>
				<!-- SE PINTA EL BOTON SOLO UNA VEZ -->
				<?php 
				if(($idPedidoOriginal != $value['id_pedido']) || $i==1) {
					//MONTO TOTAL POR CADA PEDIDO
					$montoTotalPedido = $value['monto_pedido'];
					$idPedidoOriginal = $value['id_pedido'];
						if($value['estatus'] == 2){
							?>
							<td class="modo2">Entregado</td>
							<?php 
						}else {
							?>	
							<td class="modo2"><input type="checkbox" class="entregado" name="entregado_<?php echo $value['id_pedido']; ?>" id="<?php echo $value['id_pedido']; ?>" value="entregado_<?php echo $value['id_pedido']; ?>"></td>
							<?php 
						}
				} 
				?>
			</tr>
			<?php
			$i++;
		
			//SI ES EL ULTIMO REGISTRO PINTO EL TOTAL DEL ULTIMO PEDIDO
			if($i-1 == count($resultado)){
				?>
			 		<tr class="modo1"><td class="modo2">Total : <?php echo number_format($montoTotalPedido, 2, ",", ".");  ?></td></tr>
			 	<?php 
			}
		}	
		?>
		</table>
	</div>
	<script type='text/javascript' src="../../../js/ajax.js"></script>
<?php } else { ?>
	<div id="listapedidosfinal"></div>
<?php 
} 
?>
