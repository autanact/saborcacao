<?php
session_start();
if (!isset($_SESSION['detalle'])){
	$_SESSION['detalle'] = array();	
}
//header("Cache-Control: no-cache, must-revalidate");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Sabor Cacao</title>
	<meta name="Sabor Cacao" content=""/>
	<?php
	/*
	 * Created on 15/06/2009
	 *
	 * To change the template for this generated file go to
	 * Window - Preferences - PHPeclipse - PHP - Code Templates
	 */
	if (file_exists('../conf.php')){
		include('../conf.php');
	}else{
		include('plantillas/conf.php');
	}
	require_once (DIR_WS_CLASES."Funciones.php");
	require_once (DIR_WS_CLASES."Usuario.php");
	
	print("<link rel='StyleSheet' href='".DIR_WS_CSS."estilos.css' type='text/css'/>");
	print("<link rel='StyleSheet' href='".DIR_WS_CSS."calendar.css' type='text/css'/>");
	print("<link rel='StyleSheet' href='".DIR_WS_CSS."fonts.css' type='text/css'/>");
	
	//ESTILO PARA CALENDARIO
	print("<link rel='StyleSheet' href='".DIR_WS_CSS."jquery-ui.css' type='text/css'/>");
	?>
	
	<script type="text/javascript" src="../../js/jquery.js"></script>
	<!-- <script type="text/javascript" src="../../js/jquery-1.9.1.min.js"></script> 
	<script type="text/javascript" src="../../js/jquery-1.12.3.min.js"></script>
	 -->
	 
	<script type="text/javascript" src="../../js/alertify/lib/alertify.min.js"></script>
	<script type='text/javascript' src="../../js/ajax.js"></script>
	<script type='text/javascript' src="../../js/funciones.js"></script>
	<script type='text/javascript' src="../../js/jquery-ui.js"></script>
	<script type='text/javascript' src="../../js/jquery.numeric.js">
	
	<!-- SE INICIALIZA EL DATE PICKER PARA LA FECHA -->
	<script>
	
	$(function () {
	$("#datepicker").datepicker();
	});
	</script>

	<!-- Bootstrap -->
	<link href="../../css/bootstrap.css" rel="stylesheet">
	<script  src="../../js/bootstrap.min.js"></script>
	
	 <!-- Alertity -->
	<link rel="stylesheet" href="../../js/alertify/themes/alertify.core.css" />
	<link rel="stylesheet" href="../../js/alertify/themes/alertify.bootstrap.css" id="toggleCSS" />
	<script src="../../js/alertify/lib/alertify.min.js"></script>
</head>
<body>
	<div id='contenedor_principaladmin'>
		<div id='head'>
		</div> <!--//Se cierra el DIV del head-->
		<div id='contenedor_centraladmin'> <!--SE COMIENZA EL CONTENEDOR CENTRAL-->