<?php
session_start();
if(isset($_GET["page"])){
	$page=$_GET["page"];
}else{
	$page=0;
}

switch($page){

	case 1:
		$json = array();
		$json['msj'] = 'Producto Agregado';
		$json['success'] = true;
	
		if (isset($_POST['idProducto']) && $_POST['idProducto']!='') {
		try {
			$idProducto   = $_POST['idProducto'];
			$producto     = $_POST['producto'];
			$unidad       = $_POST['unidad'];
			
			$_SESSION['detalle'][$idProducto] = array('idProducto'=>$idProducto, 'producto'=>$producto, 'unidad'=>$unidad);
			
			$json['success'] = true;
			
			echo json_encode($json);
		
		} catch (PDOException $e) {
			$json['msj'] = $e->getMessage();
			$json['success'] = false;
			echo json_encode($json);
		}
		}else{
			$json['msj'] = 'Ingrese un producto y/o ingrese cantidad';
			$json['success'] = false;
			echo json_encode($json);
		}
	break;

	case 2:
		$json = array();
		$json['msj'] = 'Producto Eliminado';
		$json['success'] = true;
	
		if (isset($_POST['idProducto'])) {
			try {
				unset($_SESSION['detalle'][$_POST['idProducto']]);
				$json['success'] = true;
	
				echo json_encode($json);
	
			} catch (PDOException $e) {
				$json['msj'] = $e->getMessage();
				$json['success'] = false;
				echo json_encode($json);
			}
		}
	break;
}
?>