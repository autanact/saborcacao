<?php
if (file_exists('conf.php')){
	include('conf.php');
}else{
	include('plantillas/conf.php');
}
include_once (DIR_WS_CLASES."Pedidos.php");
include_once (DIR_WS_CLASES."Conexion.php");

$conectado = new Conexion();
$conectado->abrir_conexion();
//LA FUNCION PRINCIPAL DE LA FUNCION LIMPIAR VARIABLE CONSISTE EN UTILIZAR LA FUNCION mysql_real_escape_string PARA EVITAR INJECCION SQL
//CAPTURO LOS DATOS DEL FORMULARIO/
$idPedido = limpiar_variable($_POST['idPedido']);
$conectado->cerrar_conexion();

$pedidos = new Pedidos();
$estatus = 2;
//SE COLOCA EL ESTATUS 2 QUE SIGNIFICA ENTREGADO
$resultado = $pedidos->actualizarPedidoFinal($idPedido,$estatus);

if($resultado){
	echo "Operaci&oacute;n Realizada con &Eacute;xtio";
}else {
	echo "Ocurrio un error al momento de realizar la operacion";
}

?>