<?php
if (file_exists('conf.php')){
	include('conf.php');
}else{
	include('plantillas/conf.php');
}
include_once (DIR_WS_CLASES."Pedidos.php");
include_once (DIR_WS_CLASES."Conexion.php");

$conectado = new Conexion();
//CAPTURO LOS DATOS DEL FORMULARIO/
$conectado->abrir_conexion();
//LA FUNCION PRINCIPAL DE LA FUNCION LIMPIAR VARIABLE CONSISTE EN UTILIZAR LA FUNCION mysql_real_escape_string PARA EVITAR INJECCION SQL
$idtipo     = limpiar_variable($_POST['idtipo']);
$cantidad   = limpiar_variable($_POST['cantidad']);
$conectado->cerrar_conexion();

$pedidos = new Pedidos();
$resultado = $pedidos->consultarPrecios($cantidad,$idtipo);

if($resultado){
	$minprecio = number_format($resultado[0]['minprecio'], 2, ",", ".");
	$maxprecio = number_format($resultado[0]['maxprecio'], 2, ",", ".");
	
	$maxminprecio = $minprecio."*".$maxprecio;
	echo $maxminprecio; 
}else {
	echo "ERROR";
}
?>