<?php
@session_start();
if (file_exists('conf.php')){
	include('conf.php');
}else{
	include('plantillas/conf.php');
}
include_once (DIR_WS_CLASES."Clientes.php");
include_once (DIR_WS_CLASES."Conexion.php");

$conectado = new Conexion();
$conectado->abrir_conexion();
//LA FUNCION PRINCIPAL DE LA FUNCION LIMPIAR VARIABLE CONSISTE EN UTILIZAR LA FUNCION mysql_real_escape_string PARA EVITAR INJECCION SQL
//CAPTURO LOS DATOS DEL FORMULARIO/
$telefono = limpiar_variable($_POST['txt_telefono']);
$email    = limpiar_variable($_POST['txt_email']);
$codigo   = limpiar_variable($_POST['txt_codigo']);

//SE CAPTURAN LOS PRODUCTOS QUE SELECCIONO EL USUARIO
$productos      = limpiar_variable($_POST['productos']);
$conectado->cerrar_conexion();


$clientes = new Clientes();
$resultado = $clientes->registrarClienteInicial($telefono,$email,$codigo,$productos);

if($resultado=="true"){
	echo "<br><h2>Su pedido ha sido generado exitosamente</h2> ";
	session_destroy();
	/*SI EL PEDIDO SE REGISTRA CORRECTAMENTE SE ENVIA UN CORREO CON LA INFORMACION*/
	
	//SE TRANSFORMA AL ARREGLO PRINCIPAL DE PRODUCTOS HACIENDO UN EXPLODE POR EL CARACTER *
	$arrayProductos = explode("*", $productos);
	$totalRegistros = count($arrayProductos);
	
	//echo "total registros es: "+$totalRegistros;
	$detallePedido="<table style='border: #C00000 1px solid; border-collapse: collapse;'><tr style='border: #C00000 1px solid;'><th style='border: #C00000 1px solid;'>Producto</th style='border: #C00000 1px solid;'><th style='border: #C00000 1px solid;'>Cantidad</th></tr>";
	for ($i = 0; $i < $totalRegistros; $i++) {
		//SE CONVIERTE EN UN ARRAY EN LA PRIMERA POSICION QUEDA EL ID DEL PRODUCTO Y EN LA SEGUNDA LA CANTIDAD
		$array2 = explode(",",$arrayProductos[$i]);
		//print_r($array2);
		$idProducto = $array2[0];
		$cantidad   = $array2[1];
		$producto   = $array2[2];
		//SE CREA EL QUERY PARA LA TABLA PEDIDOS INICIALES
		$detallePedido .= "<tr style='border: #C00000 1px solid;'><td style='border: #C00000 1px solid;'>".$producto."</td><td style='border: #C00000 1px solid;'>".$cantidad."</td></tr>";
	}
	$detallePedido.= "</table>";
	
	//$rutaImagenes  = DIR_WS_IMAGENES;
	//echo "ruta imagenes ".$rutaImagenes;
	
	$titulo    = 'PEDIDO SABOR CACAO';
	
	$mensaje   = "Estimado cliente,<br><br>
		    	  Gracias por preferir los productos de Sabor Cacao.:<br><br>
				  El detalle de su pedido es el siguiente: <br><br>".
				  $detallePedido.
				 "<br>Estaremos confirmando su pedido a través del siguiente número: ".$telefono.	
		         "<br><br>Saludos cordiales,<br>
				  <br>
				  <table>
					<tr> 
						<td>
							<img src='http://www.saborcacao.com/imagenes/logoSaborCacao.png' height='150'>
						</td>
						<td>
							<b>Informaci&oacute;n</b><br>
							Sabor Cacao<br><br>

							CR 44 # 60 Sur – 35 TO 1 OF 2404 – Medellín, Colombia.<br><br>

							+57 4 579.92.66<br>
							+57 313 517.38.34<br><br>

							<a href='mailto:informacion@saborcacao.com'>informacion@saborcacao.com</a><br>
							<a href='http://www.saborcacao.com' target='_blank'>www.saborcacao.com</a> 	
						</td> 
					<tr>
				 </table>";
	
	//echo "mensaje es ".$mensaje;
	
	// Para enviar un correo HTML, debe establecerse la cabecera Content-type
	$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
	$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	
	// Cabeceras adicionales
	$cabeceras .= 'To: informacion@saborcacao.com'. "\r\n";
	$cabeceras .= 'From: SABOR CACAO <notificaciones@saborcacao.com>' . "\r\n";
	
	if(!mail($email, $titulo, $mensaje, $cabeceras)){
		echo "<h2>Ocurrio un problema al enviar el correo</h2>";
	}
} else if($resultado=="CODIGO_NO_ENCONTRADO"){
	echo "<br><h2>Ocurrio un problema con el codigo introducido.</h2> ";
	session_destroy();
} else {
	echo "<br><h2>Ocurrio un problema al realizar a operacion. Por favor intente mas tarde.</h2> ";
}
?>