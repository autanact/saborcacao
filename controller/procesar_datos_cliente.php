<?php
if (file_exists('conf.php')){
	include('conf.php');
}else{
	include('plantillas/conf.php');
}

include_once (DIR_WS_CLASES."Clientes.php");
include_once (DIR_WS_CLASES."Conexion.php");

$conectado = new Conexion();
$conectado->abrir_conexion();
//CAPTURO LOS DATOS DEL FORMULARIO/
//LA FUNCION PRINCIPAL DE LA FUNCION LIMPIAR VARIABLE CONSISTE EN UTILIZAR LA FUNCION mysql_real_escape_string PARA EVITAR INJECCION SQL
$nombrecliente = limpiar_variable($_POST['nombrecliente']);
$nit           = limpiar_variable($_POST['nit']);
$telefono      = limpiar_variable($_POST['telefono']);
$direccion     = limpiar_variable($_POST['direccion']);
$ciudad        = limpiar_variable($_POST['ciudad']);
$municipio     = limpiar_variable($_POST['municipio']);
$estado        = limpiar_variable($_POST['estado']);
$pais          = limpiar_variable($_POST['pais']);
$tipopersona   = limpiar_variable($_POST['tipopersona']);
$email         = limpiar_variable($_POST['email']);
$codigo        = limpiar_variable($_POST['codigo']);
$idcliente     = limpiar_variable($_POST['idcliente']);
$tipooperacion = limpiar_variable($_POST['tipooperacion']);
$emailoriginal = limpiar_variable($_POST['emailoriginal']);
$conectado->cerrar_conexion();

$clientes = new Clientes();

if(!isset($nit) || $nit==""){
	$nit =  "NULL";
}else {
	$nit = "'".$nit."'";
}

if(!isset($codigo) || $codigo==""){
	$codigo =  "NULL";
}else {
	$codigo = "'".$codigo."'";
}

//SI TIPO DE OPERACION ES 1 SE VA A GUARDAR EL CLIENTE POR PRIMERA VEZ
if($tipooperacion == "1"){
	$resultado = $clientes->registrarCliente($nombrecliente, $nit, $telefono, $direccion, $ciudad, $municipio,$estado,$pais,$tipopersona,$email,$codigo);
//SI TIPO DE OPERACION ES 2 SE VA A REALIZAR UNA ACTUALIZACION DE TODOS LOS DATOS DEL CLIENTE
}else if($tipooperacion == "2") {
	$resultado = $clientes->actualizarCliente($emailoriginal,$idcliente,$nombrecliente, $nit, $telefono, $direccion, $ciudad, $municipio,$estado,$pais,$tipopersona,$email,$codigo);
}

if($resultado == "true"){
	echo "true";
}else {
	//SE DEVULEVE MENSAJE DE ERROR
	echo $resultado;
}

?>