<?php
@session_start();
if (file_exists('conf.php')){
	include('conf.php');
}else{
	include('plantillas/conf.php');
}

include_once (DIR_WS_CLASES."Funciones.php");
include_once (DIR_WS_CLASES."Usuario.php");
include_once (DIR_WS_CLASES."Conexion.php");

$conectado = new Conexion();
$conectado->abrir_conexion();
//LA FUNCION PRINCIPAL DE LA FUNCION LIMPIAR VARIABLE CONSISTE EN UTILIZAR LA FUNCION mysql_real_escape_string PARA EVITAR INJECCION SQL
//SE CAPTURAN LOS PRODUCTOS QUE SELECCIONO EL USUARIO
$login        = limpiar_variable_sin_toupper($_POST['usuario']);
$clave        = limpiar_variable_sin_toupper($_POST['clave']);
$conectado->cerrar_conexion();

$usuario    = new Usuario();
$resultado  = $usuario->validarusuario($login,$clave);

//EL USUARIO FUE ENCONTRADO
if(is_array($resultado)){
		foreach($resultado as $valor){
			//CREO UNA VARIABLE DE SESSION CON EL ID DEL USUARIO
			$_SESSION['id_usuario'] = $valor['id_usuario'];
			//SE ENVIA TRUE EN LA RESPUESTA AJAX
			echo "true";
		}
} else { //EL USUARIO TIENE LA CLAVE O EL LOGIN INVALIDO
	if((isset($login)) && ($login!="")) {
		$_SESSION['error']          = "<h4>Ha ocurrido un problema con sus credenciales</h4>";
	}
	//SE ENVIA FALSE EN LA RESPUESTA AJAX
	echo "false";	
}
?>