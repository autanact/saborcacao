<?php
@session_start();
if (file_exists('conf.php')){
	include('conf.php');
}else{
	include('plantillas/conf.php');
}
include_once (DIR_WS_CLASES."Pedidos.php");
include_once (DIR_WS_CLASES."Conexion.php");

$conectado = new Conexion();
$conectado->abrir_conexion();
//LA FUNCION PRINCIPAL DE LA FUNCION LIMPIAR VARIABLE CONSISTE EN UTILIZAR LA FUNCION mysql_real_escape_string PARA EVITAR INJECCION SQL
//SE CAPTURAN LOS PRODUCTOS QUE SELECCIONO EL USUARIO
$productos      = limpiar_variable($_POST['productos']);
$fechaentrega   = limpiar_variable($_POST['fechaentrega']);
$idcliente      = limpiar_variable($_POST['idcliente']);
$montototal     = limpiar_variable($_POST['montototal']);
$idregistroinicialinput = limpiar_variable($_POST['idregistroinicialinput']);
$conectado->cerrar_conexion();

$estatus = 1;
$pedidos = new Pedidos();
$resultado = $pedidos->registrarPedidoFinal($montototal,$estatus,$idregistroinicialinput,$idcliente,$fechaentrega,$productos);

if($resultado==true){
	echo "<br><h2>Su pedido ha sido generado exitosamente</h2> ";
}else {
	echo "<br><h2>Ocurrio un problema al realizar a operacion. Por favor intente mas tarde.</h2> ";
}
?>