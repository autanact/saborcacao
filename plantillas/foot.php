			</div> <!-- SE CIERRA EL CONTENEDOR CENTRAL -->
			<div id='foot'>
				<div id="contenedor1foot">
					<div id="logofoot">
						<img src="<?php echo DIR_WS_IMAGENES."logoSaborCacaoB.png" ?>">
					</div>
					<br><br>
					<b>Sabor Cacao</b> es una empresa vanguardista que nace de un equipo apasionado por el mundo del chocolate, 
					quienes tienen el prop�sito de crear productos con el mejor cacao combinado a los m�s frescos ingredientes,
			 		buscando como resultado la perfecci�n en sabor, equilibrio y textura, convirtiendo 
			 		cada preparaci�n en un <b>Arte del Sabor.</b>
					<br>
				</div>
				<div id="contenedor2foot">
					<center><p>Nosotros</p></center>
					<br><br>
					Nuestra Visi�n es ser reconocidos a nivel nacional e internacional como la empresa 
					chocolatera l�der en calidad, sabor e innovaci�n. 
					<br><br>
					Nuestra Misi�n consiste en satisfacer efectivamente cada paladar con nuestros productos
					elaborados bajo estricta selecci�n de ingredientes naturales de alta calidad. 
					<br><br><br><br>
					<div class="lineaprincipalfoot"><div class="lineafoot"></div><div class="rombofoot">&#x2666;</div><div class="lineafoot"></div></div>
				</div>
				<div id="contenedor3foot">
					<p>Redes Sociales</p>
					<br>
					
					<a class="twitter-timeline" height="100" data-chrome="nofooter transparent noheader noborders noscrollbar" data-tweet-limit="1" lang="ES" href="https://twitter.com/sabor_cacao" data-widget-id="725664168918048768">Tweets por el @sabor_cacao.</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id))
						      {js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}
				      (document,"script","twitter-wjs");
					
					</script>
					<br>
					<!-- LightWidget WIDGET --><script src="//lightwidget.com/widgets/lightwidget.js"></script>
					<iframe src="//lightwidget.com/widgets/d667ee6e2e0b5a17afc06ae74d04071e.html" id="lightwidget_d667ee6e2e" 
					name="lightwidget_d667ee6e2e"  scrolling="no" allowtransparency="true" 
					class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;">
					</iframe>

				</div>
				<div id="contenedor4foot">
				    <p>Contactos</p> 
				    E-mail: informacion@saborcacao.com 
					<br><br>
					Tel�fonos:      <br> 
					+57 4 5799266   <br>
					+57 313 5173834 <br>
					<br>
					Cr 44 # 60 Sur - 35, Medell�n, Antioquia, Colombia.
					<br>

					<script type='text/javascript'> 
						function init_map(){
							var myOptions = {
									zoom:15,
									//center:new google.maps.LatLng(6.150558999999999,-75.61681999999996),
									center:new google.maps.LatLng(6.157995, -75.608769),
									mapTypeId: google.maps.MapTypeId.ROADMAP};
							        map = new google.maps.Map(document.getElementById('mapfoot'), 
								    myOptions);
					
						 		marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(6.156961, -75.608574)});
						 		infowindow = new google.maps.InfoWindow({content:'<strong>Sabaneta</strong><br>Sabaneta, Antioquia<br>'});
					
						 		google.maps.event.addListener(marker, 'click', 
						 		 		function(){
					 		 				infowindow.open(map,marker);
						 		});
					
						 infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);
				 	</script>      
					 
					<div id="mapfoot"></div>    
				</div>
				<div id="contenedor5foot">� 2016 Autana Arte del Sabor NIT: 900.894.520-9</div>
			</div>
			
		</div> <!-- SE CIERRA EL CONTENEDOR PRINCIPAL -->
	</div> <!-- SE CIERRA EL CONTENEDOR FONDO -->
	</BODY>
</HTML>
