<?php
session_start();
if (!isset($_SESSION['detalle'])){
	$_SESSION['detalle'] = array();	
}
//header("Cache-Control: no-cache, must-revalidate");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Sabor Cacao</title>
	<meta name="Sabor Cacao" content=""/>
	<?php
	/*
	 * Created on 15/06/2009
	 *
	 * To change the template for this generated file go to
	 * Window - Preferences - PHPeclipse - PHP - Code Templates
	 */
	if (file_exists('../conf.php')){
		include('../conf.php');
	}else{
		include('plantillas/conf.php');
	}
	require_once (DIR_WS_CLASES."Funciones.php");
	
	print("<link rel='StyleSheet' href='".DIR_WS_CSS."estilos.css' type='text/css'/>");
	print("<link rel='StyleSheet' href='".DIR_WS_CSS."calendar.css' type='text/css'/>");
	print("<link rel='StyleSheet' href='".DIR_WS_CSS."fonts.css' type='text/css'/>");
	?>
	
	<script type="text/javascript" src="js/jquery.js"></script>
	<!--  <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script> 
	<script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
	 -->
	 
	<script type="text/javascript" src="js/alertify/lib/alertify.min.js"></script>
	<script type='text/javascript' src="js/ajax.js"></script>
	<script type='text/javascript' src="js/funciones.js"></script>
	
	
	<!-- SCRIPT PARA EL MAPA DE GOOGLE -->
	<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>

	<!-- Bootstrap -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<script src="js/bootstrap.min.js"></script>
	
	 <!-- Alertity -->
	<link rel="stylesheet" href="js/alertify/themes/alertify.core.css" />
	<link rel="stylesheet" href="js/alertify/themes/alertify.bootstrap.css" id="toggleCSS" />
	<script src="js/alertify/lib/alertify.min.js"></script>
	<link rel="icon" type="image/png" href="<?php print(DIR_WS_IMAGENES) ?>logoSaborCacao.png" />
</head>
<body>
<div id='contenedor_fondo'>
	<div id='contenedor_principal'>
		<div id='head'>
			<div id="banner">
				<img src="<?php print(DIR_WS_IMAGENES); ?>imagenPrincipal.jpg" />
			</div>
			<div id="menu">
				<a onclick="mostrarOcultarElementos('inicio')" id="inicio" href="#banner" style="text-decoration: underline;">Inicio</a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a onclick="mostrarOcultarElementos('productos')" id="productos" href="#menu">Productos</a>
				<div class="lineaprincipal"><div class="linea"></div><div class="rombo">&#x2666;</div><div class="linea"></div>
			</div>
		</div> <!--//Se cierra el DIV del head-->
	<div id='contenedor_central'> <!--SE COMIENZA EL CONTENEDOR CENTRAL-->