<?php 
@session_start();
if (file_exists('../conf.php')){
	include('../conf.php');
}else{
	include('plantillas/conf.php');
}

//VARIABLE QUE PERMITE SABER SI LA FUNCION ELIMINARFILA FUE LLAMADA DESDE CODIGO AJAX O DESDE CODIGO PHP
$baderaPHP=1;
?>
<!-- SE ABRE FORM -->
<form id="registroInicial" method="post">
<?php 
if(count($_SESSION['detalle'])>0){?>
		
		<input id="txt_idproducto" name="txt_cantidad" type="hidden" />
		<div id="mensajePedido"  style="visibility: hidden;">
			<br><br><br><br>
			Genera tu pedido selecionado los productos del cat&aacute;logo
			<br><br><br><br>
		</div>
		<table class="tabla" id="tblDatos">
	    <thead>
	        <tr>
	            <th>Descripci&oacute;n</th>
	            <th>Cantidad</th>
	            <th></th>
	        </tr>
	    </thead>
	 	<tbody>
    	<?php 
    	foreach($_SESSION['detalle'] as $k => $detalle){ 
    	?>
	        <tr class="modo1" id="<?php echo $detalle['idProducto']; ?>">
	        	<td><?php echo utf8_decode($detalle['producto']);?></td>
	            <td class="modo2">
		            <div class="cantidadInput">
		            	<input value="<?php echo utf8_decode($detalle['producto']); ?>" id="nombre_producto_<?php echo $detalle['idProducto']; ?>"  type="hidden" />
		            	<input value='1' onKeypress="return validarNumero(event)" type="text" id="cantidad_<?php echo $detalle['idProducto']; ?>"  maxlength="4" />
		            </div>
		            <div class="incrementaDecrementa">
			            <a onclick="aumentarCantidad('<?php echo $detalle['idProducto']; ?>')"><img src="<?php echo DIR_WS_IMAGENES."incrementa.png" ?>"></a>
			            <br>
			            <a onclick="disminuirCantidad('<?php echo $detalle['idProducto']; ?>')"><img src="<?php echo DIR_WS_IMAGENES."decrementa.png" ?>"></a>
    				</div>
    				<div class="unidades"> 
    					<?php echo $detalle['unidad'];?>
					</div>
				</td>
	            <td>
	            <a class="eliminar-producto"  id="eliminar_<?php echo $detalle['idProducto']; ?>"
							onclick="eliminarFila('<?php echo $detalle['idProducto']; ?>',this,<?php echo $baderaPHP; ?>)">
							<img src="<?php echo DIR_WS_IMAGENES."icono_menos.png" ?>">
							</a>
	          	</td>
	        </tr>
        <?php 
    	 }
		 ?>
    	</tbody>
	 </table>
	 <div id="generar_pedido" style="visibility: visible;">
		 <table class="tabla">   
		    <tr class="modo1">
		    	<td>* Tel&eacute;fono</td>
				<td colspan="2">
					<input id="txt_telefono"  name="txt_telefono"   type="text" value="+57" onKeypress="return validarNumero(event)" maxlength="13"/>
				</td>
		    </tr>
		     <tr class="modo1">
		    	<td>* Email</td>
				<td colspan="2"><input id="txt_email"  name="txt_email"  type="text" required/></td>
		    </tr>
		    <tr class="modo1">
		    	<td>C&oacute;digo</td>
				<td colspan="2"><input id="txt_codigo"  name="txt_codigo"  type="text" /></td>
		    </tr>
		    <tr>
		    	<th colspan="3" ><button type="submit" class="boton btn-generar-pedido" id="generarpedidoboton" >Generar Pedido</button></th>
		    </tr>
		</table>
	</div>
<?php 
}else {?>
<div id="mensajePedido"  style="visibility: visible;">
	<br><br><br><br>
	Genera tu pedido selecionado los productos del cat&aacute;logo
	<br><br><br><br>
</div>
<!-- se crea la tabla sin datos -->
<table style="visibility: hidden;" class="tabla" id="tblDatos" >
	    <thead>
	        <tr>
	            <th>Descripci&oacute;n</th>
	            <th colspan="2">Cantidad</th>
	        </tr>
	    </thead>
</table>

<div id="generar_pedido" style="visibility: hidden;">
	<table class="tabla">   
	    <tr class="modo1">
	    	<td>* Tel&eacute;fono</td>
			<td colspan="2">
				<input id="txt_telefono"  name="txt_telefono"  type="text" value="+57" onKeypress="return validarNumero(event)" maxlength="13"/>
			</td>
	    </tr>
	     <tr class="modo1">
	    	<td>* Email</td>
			<td colspan="2"><input id="txt_email"  name="txt_email"  type="text" required/></td>
	    </tr>
	    <tr class="modo1">
	    	<td>C&oacute;digo</td>
			<td colspan="2"><input id="txt_codigo"  name="txt_codigo"  type="text" /></td>
	    </tr>
	    <tr>
	    	<th colspan="3" ><button type="submit" class="boton btn-generar-pedido" id="generarpedidoboton" >Generar Pedido</button></th>
	    </tr>
	</table>
</div>
<?php 
}
?>
</form>	
<script type='text/javascript' src="<?php echo DIR_WS_JS."jquery.numeric.js" ?>"></script>
<script type='text/javascript' src="<?php echo DIR_WS_JS."jquery.validate.js" ?>"></script>
<script type='text/javascript' src="<?php echo DIR_WS_JS."ajax.js" ?>"></script>


