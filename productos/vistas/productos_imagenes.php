<?php 
require_once (DIR_WS_CLASES."Productos.php");
?>
<div id="productos_imagenes"> 
	<!--  <h2 id="productos_imagenes"></h2>  -->
	<input id="txt_producto" name="txt_producto" type="hidden" />
	<input id="txt_cantidad" name="txt_cantidad" type="hidden" />
	<input id="txt_idproducto" name="txt_cantidad" type="hidden" />
	<input id="txt_imagen"  name="txt_imagen"  type="hidden" /> 
	<input id="txt_unidad_producto"  name="txt_unidad_producto"  type="hidden" /> 
	
	<table>
		<?php 
		$estatus = 1;
		$productos      = new Productos();
		$datosProductos = $productos->consultarProductos($estatus);
		$contador = 0;
		$i=0;
		//INDICA LA CANTIDAD DE COLUMNAS QUE TENDRAN LOS PRODUCTOS EN LA PAGINA
		$numeroColumnasProductos=3;
		foreach ($datosProductos as $valor){
			if($contador==$numeroColumnasProductos){ 
				print("</tr><tr>");
				$contador=0;
			}
			?>
			<td>
				<div class="view_principal">
			    	<div class="view_titulo"><?php echo $valor['nom_producto']; ?></div>
					<div class="view" >
						<img src="<?php echo DIR_WS_IMAGENES.$valor['url_imagen']; ?>" alt="NF" />
						<div class="check">
							 <div id="icono_check_<?php echo $valor['id_producto'] ?>"><img src="<?php echo DIR_WS_IMAGENES."check.png" ?>"></div>
						</div>

						<div class="mask">
							<br>
							<div class="sabor" style="background-color:<?php echo $valor['color_sabor']; ?>"><?php echo $valor['denom_producto']; ?></div>
							<p><?php echo $valor['desc_producto'];?></p>
							<!-- DIVS EL ICONO AREGAR -->
							<div class="icono_agregar_principal">
								<div id="icono_agregar_<?php echo $valor['id_producto'] ?>">
									<a class="btn-success btn-agregar-producto"  id="agregar_<?php echo $valor['id_producto'] ?>"
									onclick="setearValorProducto('<?php echo $valor['nom_producto']; ?>',<?php echo $valor['id_producto']; ?>,1,'<?php echo $valor['unidad_tipo']; ?>')">
									<img id="imagenAgregar_<?php echo($i); ?>" src="<?php echo DIR_WS_IMAGENES."icono_mas.png" ?>" title="Agregar">
									</a>	
								</div>
							</div>
		
						
							<!-- DIVS EL ICONO ELIMINAR -->
							<div class="icono_eliminar_principal">
								<div id="icono_eliminar_<?php echo $valor['id_producto'] ?>">
									<a class="btn-success eliminar-producto"  id="eliminar_<?php echo $valor['id_producto'] ?>"
									onclick="eliminarFila('<?php echo $valor['id_producto'] ?>',this)">
									<img id="imagenEliminar_<?php echo($i); ?>" src="<?php echo DIR_WS_IMAGENES."icono_menos.png" ?>" title="Eliminar">
									</a>
								</div>	
							</div>
							
						</div> <!-- FIN DIV MASCARA -->	
					</div> <!-- FIN DIV VIEW -->	
				</div> <!-- FIN DIV VIEW PRINCIPAL -->
			</td>
				<?php 
			$contador++;
		} //FIN DEL FOR DE PRODUCTOS    	
		?>
		</tr>
	</table>
</div>
<!-- FIN DE DIV DE productos_imagenes -->

<!-- ESTE DIV SE REGARGA AUTOMATICAMENTE DE ACUERDO AL AJAX -->
<div id="productos_compra">
	<h2>Detalle de Pedido</h2>
	<!-- DIVS PARA MOSTRAR MENSAJE DE EXITO -->
	<div class="exito-detalle-producto"></div>
	<div class="panel-body detalle-producto">
	<?php 
		require_once (DIR_WS_PRODUCTOS."productos_detalle.php");
	?>
	</div>
</div>

<?php 
	//RECORRO LA VARIABLE DE SESSION PARA ACTIVAR Y DESACTIVAR LOS DIVS DE CHECK Y ELIMINAR
	//EN CADA CAJA DE LA LISTA DE PRODUCTOS
	foreach($_SESSION['detalle'] as $k => $detalle){ 
		$idProducto = $detalle['idProducto'];
		$producto   = $detalle['producto'];
		$unidad     = $detalle['unidad'];
		echo "<script type='text/javascript'>setearValorProducto('".$producto."',".$idProducto.",'".$unidad."');</script>";	
	}		
?>

