<?php
/*
 * Created on 26/04/2009
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include_once (DIR_WS_CLASES."Funciones.php");

class Conexion{
	
	
	private $host="localhost";
	private $usuario="saborc_admin";
	private $contrasena="autanact*.45.22.";
	private $bdseleccion="saborc_base_datos";
	
	/*
	private $host="localhost";
	private $usuario="root";
	private $contrasena="";
	private $bdseleccion="saborc_base_datos";
	*/
	
	//FUNCION QUE SE CONECTA AL SERVIDOR DE BASE DE DATOS Y LUEGO A LA BASE DE DATOS
	function conectar(){
		$this->conectarMysql();
		$this->conectarBD();
	} 
	
	//Funcion para establecer la conexion con mysql
	function conectarMysql(){
		$conexion = $this->conexion=mysql_connect($this->host,$this->usuario,$this->contrasena);
		if(!$this->conexion){
			die('No se pudo realizar la conexion con Mysql: '.mysql_error());
		}else{
			return $conexion;
		}
	}

	//Funcion para establecer la conexion con la base de datos
	function conectarBD(){
		$this->bdseleccion=mysql_select_db($this->bdseleccion);
		if(!$this->bdseleccion){
			die('Error: no se pudo conectar a la base de datos.'.mysql_error());
		}
	}

	//funcion para ejecutar las sentencias dentro de la base de datos
	function ejecutarQuery($sql){
		$conexion = $this->conectarMysql();
		$this->conectarBD();
		$resultado=mysql_query($sql);
		if(!$resultado){
			$mensajeerror = mysql_error();
			error($mensajeerror);
			throw new Exception("Error al ejecutar el query: ".$mensajeerror);
		}
		mysql_close();
		return $resultado;
	}
	
	//FUNCION QUE PERMITE EJECUTAR VARIOS QUERYS Y CONTROLAR TRANSACCIONES
	//NO ABRE NI CIERRA CONEXIONES PARA CADA QUERY
	function ejecutarQueryBatch($sql){
		$resultado=mysql_query($sql);
		if(!$resultado){
			$mensajeerror = mysql_error();
			error($mensajeerror);
			throw new Exception("Error al ejecutar el query: ".$mensajeerror);
		}
		return $resultado;
	}

	function abrir_conexion () {
		$conexion = $this->conectarMysql();
		$this->conectarBD();
	}

	function cerrar_conexion () {
		mysql_close();
	}
}
?>
