<?php
include_once (DIR_WS_CLASES."Conexion.php");
class Productos{

	function consultarProductos($estatus){
	    $conectado = new Conexion();
		$cadena = "SELECT id_producto, p.nom_producto, denom_producto, 
					desc_producto, url_imagen, color_sabor, t.unidad_tipo
					FROM clasificacion c, tipos t, productos p 
					where c.id_clasificacion = t.id_clasificacion
					and   t.id_tipos = p.id_tipo
					and   c.nom_clasificacion like '%Chocolate%'
					and   p.act_producto = ".$estatus."
					order by c.nom_clasificacion, p.id_tipo ,p.nom_producto";
		
		$resultado = $conectado->ejecutarQuery($cadena);
		if($resultado)
		{
			$resultado = mysql_fetch_all($resultado);
			//return print_r($resultado);
			return $resultado;
		}else
		{
			print("No se encontraron productos");
		}
	}
	
	function consultarPreciosProductos($idProducto, $cantidad){
	    $conectado = new Conexion();
		$cadena = "SELECT lp.maxprecio 
				   FROM   listaprecios lp
				   WHERE  
						  ".$cantidad." >= lp.cantidadini   
				   AND    ".$cantidad." <= lp.cantidadfin
				   AND    lp.id_producto = ".$idProducto;
		
		$resultado = $conectado->ejecutarQuery($cadena);
		if($resultado)
		{
			$resultado = mysql_fetch_all($resultado);
			//return print_r($resultado);
			return $resultado;
		}else
		{
			print("No se encontraron productos");
		}
	}
}
?>