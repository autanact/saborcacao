<?php
include_once ("Conexion.php");
include_once ("Funciones.php");

/*
include_once (DIR_WS_CLASES."Conexion.php");
include_once (DIR_WS_CLASES."Funciones.php");
*/

class Pedidos{
	function consultarPedidosIniciales($idregistroinicial){
		$conectado = new Conexion();
		$cadena = " SELECT p.nom_producto, pi.id_pedido_inicial, pi.id_registro_inicial, pi.id_producto, pi.cant_producto, pi.fecha_pedido,
					       tp.unidad_tipo, tp.id_tipos 
					FROM productos p, 
						 pedidosiniciales pi, 
						 tipos tp
					WHERE tp.id_tipos    = p.id_tipo
					AND   p.id_producto  = pi.id_producto
					AND   pi.id_registro_inicial= ".$idregistroinicial."
					AND   pi.estatus = 1 ";
					//ESTATUS 1 IGUAL A NO REVISADO
								
		$resultado = $conectado->ejecutarQuery($cadena);
		
		if($resultado)
		{
			$resultado = mysql_fetch_all($resultado);
			return $resultado;
		}else
		{
			print("Hubo un error en la consulta");
		}
	}
	
	//EN BASE A LA CANTIDAD ENVIADA DEVULEVE EL PRECIO MAXIMO Y MINIMO AGRUPADO 
	//POR TIPO DE UNIDAD
	function consultarPedidosInicialesTotal($idregistroinicial){
		$conectado = new Conexion();
		$cadena = " SELECT tp.id_tipos, tp.unidad_tipo, SUM( pi.cant_producto ) AS cant_producto, lp.minprecio, lp.maxprecio
					FROM productos p, pedidosiniciales pi, tipos tp, listaprecios lp
					WHERE tp.id_tipos = lp.id_tipo
					AND tp.id_tipos = p.id_tipo
					AND p.id_producto = pi.id_producto
					AND pi.id_registro_inicial = ".$idregistroinicial."
					AND pi.estatus =1
					AND pi.cant_producto >= lp.cantidadini
					AND pi.cant_producto <= lp.cantidadfin
					GROUP BY p.id_tipo ";
					//ESTATUS 1 IGUAL A NO REVISADO
											
		$resultado = $conectado->ejecutarQuery($cadena);
		
		if($resultado)
		{
			$resultado = mysql_fetch_all($resultado);
			return $resultado;
		}else
		{
			print("Hubo un error en la consulta");
		}
	}
	
	//EN BASE A LA CANTIDAD ENVIADA DEVULEVE EL PRECIO MAXIMO Y MINIMO 
	//POR TIPO DE UNIDAD
	function consultarPrecios($cantidad,$idtipo){
		$conectado = new Conexion();
		$cadena = " SELECT * FROM listaprecios lp 
					WHERE lp.id_tipo = ".$idtipo." 
					AND   ".$cantidad." >= lp.cantidadini
					AND   ".$cantidad." <= lp.cantidadfin";

		$resultado = $conectado->ejecutarQuery($cadena);
		
		if($resultado)
		{
			$resultado = mysql_fetch_all($resultado);
			return $resultado;
		}else
		{
			print("Hubo un error en la consulta");
		}
	}
	
	function consultarRegistroInicial($estatus){
		$conectado = new Conexion();
		$cadena = " SELECT id_registro_inicial,tel_cliente,email_cliente,codigo_cliente,fecha_hora 
					FROM registroinicial WHERE estatus=".$estatus." ORDER BY id_registro_inicial DESC; ";
		
		$resultado = $conectado->ejecutarQuery($cadena);
		if($resultado)
		{
			$resultado = mysql_fetch_all($resultado);
			return $resultado;
		}else
		{
			print("Hubo un error en la consulta");
		}
	}
	
	function actualizarPedidoFinal($idPedido,$estatus){
		$conectado = new Conexion();
		$cadena = " UPDATE pedidos SET estatus=".$estatus." WHERE id_pedido=".$idPedido;
		
		$resultado = $conectado->ejecutarQuery($cadena);
		if($resultado)
		{
			$resultado = mysql_fetch_all($resultado);
			return $resultado;
		}else
		{
			print("Hubo un error en la consulta");
		}
	}
	
	function eliminarPedidoInicial($idregistroinicialinput,$estatus){
		//SE ENVIA ESTATUS 3 QUE ES ELIMINADO
		$resultado = true;
		$conectado = new Conexion();
		//SE ABRE LA CONEXION
		$conectado->conectar();
		
		$cadena              = " UPDATE registroinicial   SET estatus=".$estatus." WHERE id_registro_inicial = ".$idregistroinicialinput;
        $updatePedidoInicial = " UPDATE pedidosiniciales  SET estatus=".$estatus." WHERE id_registro_inicial = ".$idregistroinicialinput;
				
	 	try
        {
         	//SE MANEJA LA TRANSACCIONALIDAD DE LA OPERACION SI FALLA ALGUNA OPERACION SE REALIZA UN ROLLBACK
        	$conectado->ejecutarQueryBatch("BEGIN");
       		$conectado->ejecutarQueryBatch($cadena);
			$conectado->ejecutarQueryBatch($updatePedidoInicial);
			$conectado->ejecutarQueryBatch("COMMIT");
        }catch(Exception $e) {
        	$conectado->ejecutarQueryBatch("ROLLBACK");
        	//SI SE PRODUCE UN ERROR EN ALGUNO DE LOS DOS QUERYS REALIZO UN ROLLBACK
			$conectado->cerrar_conexion();
			$resultado=false;
        }
	}
	
	function registrarPedidoFinal($montototal,$estatus,$idregistroinicialinput,$idcliente,$fechaentrega,$productos){
		//SE TRANSFORMA LA FECHA A ANIO MES DIA PARA GUARDAR EN LA BD		
		$fechaentrega = date("Y-m-d", strtotime($fechaentrega));
		
		$resultado=true;
		//TABLA DE BASE DE DATOS DE DONDE SE VA A BUSCAR EL ID UNICO PARA LOS INSERTS 
		$tabla = "pedidos";
		$cadenaDetallePedido = "";
		$cadenaAutoIncrement = crearQueryAutoIncrement($tabla);
	 	$conectado = new Conexion();
	 	$conectado->conectar();
		
		$resultado = $conectado->ejecutarQueryBatch($cadenaAutoIncrement);
		$resultado = mysql_fetch_array($resultado, MYSQL_ASSOC);
		$idPedido  = $resultado['AUTO_INCREMENT'];
		
		//INSERT PARA LA TABLA REGISTRO INICIAL
	 	$cadena = "INSERT INTO pedidos(id_pedido,id_cliente,fecha_entrega_pedido,monto_pedido, estatus) VALUES (".$idPedido.",".$idcliente.",'".$fechaentrega."',".$montototal.",".$estatus.");";
	 	$cadenaDetallePedido .= " INSERT INTO detallepedidos (id_pedido, id_producto, cant_producto, pvp_producto) VALUES";
		$cadenaupdatereginicial = "UPDATE registroinicial SET estatus=2 WHERE id_registro_inicial = ".$idregistroinicialinput;
		
		//SE TRANSFORMA AL ARREGLO PRINCIPAL DE PRODUCTOS HACIENDO UN EXPLODE POR EL CARACTER *
		$arrayProductos = explode("*", $productos);
		$totalRegistros = count($arrayProductos);
		
		//echo "cadena : ".$cadena;
		
		$j=0;
		for ($i = 0; $i < $totalRegistros; $i++) {
			//SE CONVIERTE EN UN ARRAY EN LA PRIMERA POSICION QUEDA EL ID DEL PRODUCTO Y EN LA SEGUNDA LA CANTIDAD
			$array2 = explode(",",$arrayProductos[$i]);
			//print_r($array2);
			$idProducto       = $array2[0];
			$cantidad         = $array2[1];
			$precio           = $array2[2];
			$cantidadOriginal = $array2[3];
			
			//SE VERIFICA SI EL PEDIDO ES POR LA CANTIDAD ORIGINAL SOLICITADA POR EL USUARIO
			if($cantidadOriginal  > $cantidad){
				
				//SE PIDIERON MENOS UNIDADES DE LAS QUE ESTABAN EN EL PEDIDO ORIGIANL
				//A LA CANTIDAD ORIGIANAL SE LE RESTA LA CANTIDAD DEL PEDIDO
				$cantidaactualizar = $cantidadOriginal-$cantidad;
				
				
				
				//SE ARMA EL UPDATE EN BASE A LA CANTIDAD A ACTUALIZAR
				$actualizardetallereginicial= " UPDATE pedidosiniciales SET cant_producto = ".$cantidaactualizar." 
				                                 WHERE id_registro_inicial = ".$idregistroinicialinput." 
				                                 AND   id_producto         = ".$idProducto.";";
				//$array = ['0'][];
				$arr[$j] = $actualizardetallereginicial;
				$j++;
			}
			
			//SE CREA EL QUERY PARA LA TABLA PEDIDOS INICIALES
			if($i+1 == $totalRegistros){
				//SI ES EL ULTIMO REGISTRO NO AGREGO LA COMA A LA CADENA
				$cadenaDetallePedido .= " (".$idPedido.",".$idProducto.",".$cantidad.",".$precio.");";
			}else {
				$cadenaDetallePedido .= " (".$idPedido.",".$idProducto.",".$cantidad.",".$precio."), ";
			}
		}
		
	    try
        {
         	//SE MANEJA LA TRANSACCIONALIDAD DE LA OPERACION SI FALLA ALGUNA OPERACION SE REALIZA UN ROLLBACK
        	$conectado->ejecutarQueryBatch("BEGIN");
       		$conectado->ejecutarQueryBatch($cadena);
			$conectado->ejecutarQueryBatch($cadenaDetallePedido);
			
			if(count($arr) > 0){
				//EL ARREGLO TIENE REGISTROS, PROCEDO A RECORRERLO
				for ($i = 0; $i < count($arr); $i++) {
					$conectado->ejecutarQueryBatch($arr[$i]);
				}
			}else {
				$conectado->ejecutarQueryBatch($cadenaupdatereginicial);
			}
			$conectado->ejecutarQueryBatch("COMMIT");
        }catch(Exception $e) {
        	$conectado->ejecutarQueryBatch("ROLLBACK");
        	//SI SE PRODUCE UN ERROR EN ALGUNO DE LOS DOS QUERYS REALIZO UN ROLLBACK
			$conectado->cerrar_conexion();
			$resultado=false;
        }
		
		$conectado->cerrar_conexion();
		
		//echo "resultado es: ".$resultado;
		return $resultado;
	}
	
	function consultarPedidosFinal($idCliente,$estatus){
		$conectado = new Conexion();
		$condicionestatus = "";
		
		if($estatus != ""){
			$condicionestatus = " AND   estatus     =".$estatus."";
		}
		
		$cadena = " SELECT p.id_pedido,
					       pro.nom_producto, 
					       dp.cant_producto, 
					       dp.pvp_producto,
					       p.fecha_pedido,
					       p.fecha_entrega_pedido, 
					       p.monto_pedido, 
					       p.estatus
					FROM  productos pro, pedidos p, detallepedidos dp
					WHERE p.id_pedido     = dp.id_pedido
					AND   pro.id_producto = dp.id_producto 
					AND   p.id_cliente=".$idCliente." ".
					$condicionestatus;
		
		$resultado = $conectado->ejecutarQuery($cadena);
		if($resultado)
		{
			$resultado = mysql_fetch_all($resultado);
			return $resultado;
		}else
		{
			//print("Hubo un error en la consulta");
		}
	}
}

?>