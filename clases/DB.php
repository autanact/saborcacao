<?php
class DB{

	public $link = null;
	private static $instance;

	private function __construct(){
		$this->link = mysql_connect("localhost", "root", "") or die('No se pudo conectar al servidor');
		mysql_select_db('saborc_base_datos') or die('No se pudo seleccionar la BD d68_prueba');
	}

	public static function &getInstance(){
		if(self::$instance == null){
			self::$instance = new self;
		}
		return self::$instance;
	}

	public function query($sql, $params = array()){
		if ((is_array($params)) and (count($params) > 0)) {
			$sql = $this->escape($sql, $params);
		}
		$result = mysql_query($sql, $this->link);
		//if(!$result){
		//	die(mysql_error() . '<br/>' . $sql);
		//}
		return $result;
	}

	private function escape($str, $arr){
		foreach($arr as $k => $v){
			$str = str_replace(":$k", mysql_real_escape_string($v), $str);
		}
		return $str;
	}

	public function all($resource){
		$ret = array();
		if(is_resource($resource)){
			while ($row = mysql_fetch_assoc($resource)) {
				$ret[] = $row;
			}
		}
		return $ret;
	}
	public function close(){
		if(is_resource($this->link)){
			mysql_close($this->link);
		}
		$this->link = null;
		self::$instance = null;
	}
}
?>