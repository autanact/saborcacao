<?php
/*
 * Created on 27/04/2009
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
//require_once(DIR_WS_CLASES."Conexion.php");
require_once("Conexion.php");

function mysql_fetch_all($res) {
	$data = array();
	$entro = false;
	while ($row = mysql_fetch_array($res)){
	   $data[] = $row;
	 	$entro = true;
	}
	if ($entro === false){
		return $entro;
	}else{
		return $data;
	}
}

function obtenerFechaActual() {
	//SE CAPTURA LA FECHA ACTUAL
	$fechaActual = getdate();
	
	// La fecha separada por guiones 
	$datetime = $fechaActual['year']."-"; 
	$datetime.= $fechaActual['mon']."-"; 
	$datetime.= $fechaActual['mday']."-"; 
	// El espacio que separa la fecha de la hora 
	$datetime.= " "; 
	//Los datos de la hora 
	$datetime.= $fechaActual['hours']; 
	$datetime.= ":".$fechaActual['minutes'];
	$datetime.= ":".$fechaActual['seconds'];
	return $datetime;
}

function error($texto){
	$ddf = fopen(DIR_WS_LOGS."error.log","a");
	fwrite($ddf,"[".date("r")."] Error: $texto ".PHP_EOL);
	fclose($ddf);
}

function crearQueryAutoIncrement($tabla){
	return "SELECT AUTO_INCREMENT FROM information_schema.TABLES where TABLE_SCHEMA='saborc_base_datos' and TABLE_NAME='".$tabla."';";
}

function limpiar_variable ($variable) {
	$variable = strtoupper($variable);
	$variable = trim($variable);
	//return $variable;
	$variable = mysql_real_escape_string($variable);
	return $variable;
}

function limpiar_variable_sin_toupper ($variable) {
	$variable = trim($variable);
	//return $variable;
	$variable = mysql_real_escape_string($variable);
	return $variable;
}

?>