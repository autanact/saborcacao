<?php
/*
include_once (DIR_WS_CLASES."Conexion.php");
include_once (DIR_WS_CLASES."Funciones.php");
*/
include_once ("Conexion.php");
include_once ("Funciones.php");

class Clientes{
	function registrarClienteInicial($telefono,$email,$codigo,$productos){
		$exito="true";
		//TABLA DE BASE DE DATOS DE DONDE SE VA A BUSCAR EL ID UNICO PARA LOS INSERTS 
		$tabla = "registroinicial";
		$cadenaPedido = "";
		$cadenaAutoIncrement = crearQueryAutoIncrement($tabla);
		$datetime = obtenerFechaActual();
	 	$conectado = new Conexion();
	 	$conectado->conectar();
	 	//PERMITE SABER SI UN CODIGO FUE O NO CONSEGUIDO
	 	$flag = 0;
	 	
	 	//EL USUARIO ENVIO UN CODIGO, SE DEBE VALIDAR QUE EL MISMO EXISTA EN LA TABLA CLIENTES
	 	if($codigo!= ""){
	 		$cadenaCodigo = "SELECT * FROM clientes WHERE codigo_cliente='".$codigo."'";
	 		//echo "cadena es ".$cadenaCodigo;
	 		$resultadoCodigo = $conectado->ejecutarQueryBatch($cadenaCodigo);
	 		if($resultadoCodigo){
	 			$resultadoCodigo = mysql_fetch_array($resultadoCodigo);
	 			foreach ($resultadoCodigo as $value)
			   	{
			   	   //echo "entre";
			   	   //EL REGISTRO FUE CONSEGUIDO	
			  	   $flag = 1;
			  	   break;
			  	}
			  	//SI NO SE CONSIGUIO EL CODIGO 
	 			if($flag == 0){
	 				//EL CODIGO NO EXISTE SE DEBE RECHAZAR LA OPERACION
	 				$conectado->cerrar_conexion();
	 				return "CODIGO_NO_ENCONTRADO";
	 			}	
	 		}else {
	 			//OCURRIO UN PROBLEMA CON EL QUERY SE DEBE RECHAZAR LA OPERACION
	 			$conectado->cerrar_conexion();
	 			return "CODIGO_NO_ENCONTRADO";
	 		}
	 	}
	 	
		$resultado = $conectado->ejecutarQueryBatch($cadenaAutoIncrement);
		$resultado = mysql_fetch_array($resultado, MYSQL_ASSOC);
		$idRegistroInicial = $resultado['AUTO_INCREMENT'];
	 	$estatus = 1;
		//INSERT PARA LA TABLA REGISTRO INICIAL
	 	$cadena = "INSERT INTO registroinicial(id_registro_inicial,tel_cliente,email_cliente,codigo_cliente, estatus) VALUES (".$idRegistroInicial.",'".$telefono."','".$email."','".$codigo."',".$estatus.");";
	 	$cadenaPedido .= " INSERT INTO pedidosiniciales (id_registro_inicial, id_producto, cant_producto, estatus) VALUES";
	
	 	//echo "cadena es : ".$cadena;
	 	
		//SE TRANSFORMA AL ARREGLO PRINCIPAL DE PRODUCTOS HACIENDO UN EXPLODE POR EL CARACTER *
		$arrayProductos = explode("*", $productos);
		$totalRegistros = count($arrayProductos);
		
		//echo "total registros es: "+$totalRegistros;
		
		for ($i = 0; $i < $totalRegistros; $i++) {
			//SE CONVIERTE EN UN ARRAY EN LA PRIMERA POSICION QUEDA EL ID DEL PRODUCTO Y EN LA SEGUNDA LA CANTIDAD
			$array2 = explode(",",$arrayProductos[$i]);
			//print_r($array2);
			$idProducto = $array2[0];
			$cantidad   = $array2[1];
			//SE CREA EL QUERY PARA LA TABLA PEDIDOS INICIALES
			if($i+1 == $totalRegistros){
				//SI ES EL ULTIMO REGISTRO NO AGREGO LA COMA A LA CADENA
				$cadenaPedido .= " (".$idRegistroInicial.",".$idProducto.",".$cantidad.",".$estatus.");";
			}else {
				$cadenaPedido .= " (".$idRegistroInicial.",".$idProducto.",".$cantidad.",".$estatus."), ";
			}
		}
		
	    try
        {
         	//SE MANEJA LA TRANSACCIONALIDAD DE LA OPERACION SI FALLA ALGUNA OPERACION SE REALIZA UN ROLLBACK
        	$conectado->ejecutarQueryBatch("BEGIN");
       		$conectado->ejecutarQueryBatch($cadena);
			$conectado->ejecutarQueryBatch($cadenaPedido);
			$conectado->ejecutarQueryBatch("COMMIT");
        }catch(Exception $e) {
        	$conectado->ejecutarQueryBatch("ROLLBACK");
        	//SI SE PRODUCE UN ERROR EN ALGUNO DE LOS DOS QUERYS REALIZO UN ROLLBACK
			$conectado->cerrar_conexion();
			$exito="false";
        }
		
		$conectado->cerrar_conexion();
		return $exito;
	}
	
	function registrarContactos($nombre,$email,$asunto,$mensaje){
		$conectado = new Conexion();
		$cadena = " INSERT INTO contactos (nombre,email,asunto,mensaje) VALUES ('".$nombre."','".$email."','".$asunto."','".$mensaje."'); ";
		
		$resultado = $conectado->ejecutarQuery($cadena);
		
		if($resultado)
		{
			return $resultado;
		}else
		{
			print("Hubo un error en la consulta");
		}
	}
	
	//SE HACE UNA CONSULTA DE LOS CLIENTES POR EL EMAIL
	function consultarClientes($email,$nombrecliente,$nit,$telefono,$direccion,$ciudad,$municipio,$estado,$pais,$tipopersona,$codigo){
		$conectado = new Conexion();
		$entre = false;
		
		if(isset($email) && $email!=" " && $email!= null){
			$condicion.= "c.email_cliente = '".$email."'";
			$entre = true;
		}
		
		if(isset($telefono) && $telefono!="" && $telefono!= null){
			if($entre){
				$condicion.= " AND c.tel_cliente LIKE '%".$telefono."%'";
			}else {
				$entre = true;
				$condicion.= " c.tel_cliente LIKE '%".$telefono."%'";
			}
		}
		
		if(isset($nombrecliente) && $nombrecliente!="" && $nombrecliente!= null){
			if($entre){
				$condicion.= " AND c.nom_cliente LIKE '%".$nombrecliente."%'";
			}else {
				$entre = true;
				$condicion.= " c.nom_cliente LIKE '%".$nombrecliente."%'";
			}
			
		}
		
		if(isset($nit) && $nit!="" && $nit!=null){
			if($entre){
				$condicion.= " AND c.nit_cliente LIKE '%".$nit."%'";
			}else {
				$entre = true;
				$condicion.= " c.nit_cliente LIKE '%".$nit."%'";
			}
		}

		if(isset($direccion) && $direccion!="" && $direccion!=null){
			if($entre){
				$condicion.= " AND c.dir_cliente LIKE '%".$direccion."%'";
			}else {
				$entre = true;
				$condicion.= " c.dir_cliente LIKE '%".$direccion."%'";
			}
		}
		
		if(isset($ciudad) && $ciudad!="" && $ciudad!=null){
			if($entre){
				$condicion.= " AND c.ciu_cliente LIKE '%".$ciudad."%'";
			}else {
				$entre = true;
				$condicion.= " c.ciu_cliente LIKE '%".$ciudad."%'";
			}
		}
		
		if(isset($municipio) && $municipio!="" && $municipio!=null){
			if($entre){
				$condicion.= " AND c.mun_cliente LIKE '%".$municipio."%'";
			}else {
				$entre = true;
				$condicion.= " c.mun_cliente LIKE '%".$municipio."%'";
			}
		}
		
		if(isset($estado) && $estado!="" && $estado!=null){
			if($entre){
				$condicion.= " AND c.est_cliente LIKE '%".$estado."%'";
			}else {
				$entre = true;
				$condicion.= " c.est_cliente LIKE '%".$estado."%'";
			}
		}
		
		if(isset($pais) && $pais!="" && $pais!=null){
			if($entre){
				$condicion.= " AND c.pais_cliente LIKE '%".$pais."%'";
			}else {
				$entre = true;
				$condicion.= " c.pais_cliente LIKE '%".$pais."%'";
			}
		}
		
		if(isset($tipopersona) && $tipopersona!=""  && $tipopersona!=null){
			if($entre){
				$condicion.= " AND c.tip_cliente = ".$tipopersona;
			}else {
				$entre = true;
				$condicion.= " c.tip_cliente = ".$tipopersona;
			}
		}
		
		if(!isset($codigo) && $codigo!="" && $codigo!=null){
			if($entre){
				$condicion.= " AND c.codigo_cliente LIKE '%".$codigo."%'";
			}else {
				$entre = true;
				$condicion.= " c.codigo_cliente LIKE '%".$codigo."%'";
			}
		}
		
		$cadena = " SELECT 
				   c.id_cliente, 
				   c.nom_cliente, 
				   c.nit_cliente, 
				   c.tel_cliente, 
				   c.dir_cliente, 
				   c.ciu_cliente, 
				   c.mun_cliente, 
				   c.est_cliente, 
				   c.pais_cliente,
				   c.tip_cliente,
				   c.email_cliente,
				   c.codigo_cliente
			FROM  clientes c ";
		
		//SE VAN A AGREGAR CONDICIONES
		if($entre){
			$cadena.= " WHERE ";
		}else  {
			//PARA QUE NO CONSIGA A NINGUN CLIENTE
			$cadena.= " WHERE c.id_cliente = -1";
		}
		
		//SE CONCATENA LA CADENA CON LA CONDICION
		$cadena.= $condicion;
		
		$resultado = $conectado->ejecutarQuery($cadena);
		if($resultado)
		{
			$resultado = mysql_fetch_all($resultado);
			//print_r($resultado);
			return $resultado;
		}else
		{
			print("Hubo un error en la consulta");
		}
	}
	
	//FUNCION QUE SE ENCARGA DE REGISTRAR A UN CLIENTE
	function registrarCliente($nombrecliente, $nit, $telefono, $direccion, $ciudad, $municipio,$estado,$pais,$tipopersona,$email,$codigo){
		$conectado = new Conexion();
		$cadena = " INSERT INTO clientes (nom_cliente,nit_cliente,tel_cliente,dir_cliente,ciu_cliente,mun_cliente,
		                                  est_cliente,pais_cliente,tip_cliente,email_cliente,codigo_cliente) VALUES 
		            ('".$nombrecliente."',".$nit.",'".$telefono."','".$direccion."','".$ciudad."','".$municipio."',
		             '".$estado."','".$pais."','".$tipopersona."','".$email."',".$codigo."
		            ); ";
	
		try
        {
			$resultado = $conectado->ejecutarQuery($cadena);
         }catch(Exception $e) {  
         	$conectado->ejecutarQueryBatch("ROLLBACK"); 	        	
			
         	//SE VERIFICA CUAL FUE EL ERROR
         	if(strpos($e, "uc_tel_cliente")){
         		$resultado = "Error: Tel&eacute;fono ya registrado en la base de datos";
         	}else if (strpos($e, "uc_codigo_cliente")) {
         		$resultado = "Error: C&oacute;digo de cliente ya registrado en la base de datos";
         	}else if (strpos($e, "uc_email_cliente")) {
         		$resultado = "Error: Email del cliente ya registrado en la base de datos";
         	}else {
         		$resultado = "Ocurri&oacute; un error inesperado al realizar la operaci&oacute;n";
         	}
        }
		return $resultado;
	}
	
	//FUNCION QUE SE ENCARGA DE REGISTRAR A UN CLIENTE
	function actualizarCliente($emailoriginal,$idcliente, $nombrecliente, $nit, $telefono, $direccion, $ciudad, $municipio,$estado,$pais,$tipopersona,$email,$codigo){
		$resultado="true";
		$conectado = new Conexion();
	 	$conectado->conectar();
	 	
		$cadena = " UPDATE clientes SET nom_cliente= '".$nombrecliente."', nit_cliente = ".$nit.", tel_cliente = '".$telefono."',   
					dir_cliente = '".$direccion."', ciu_cliente = '".$ciudad."', mun_cliente = '".$municipio."', est_cliente = '".$estado."', 
					pais_cliente = '".$pais."', tip_cliente = '".$tipopersona."', email_cliente = '".$email."', codigo_cliente = ".$codigo."
					WHERE id_cliente = ".$idcliente;

		$cadenaUpdateRInicial = "UPDATE registroinicial SET email_cliente = '".$email."', tel_cliente = '".$telefono."', codigo_cliente=".$codigo."  WHERE email_cliente = '".$emailoriginal."'";	
		
		//echo "cadena es: ".$cadena;
		
	 	try
        {
         	//SE MANEJA LA TRANSACCIONALIDAD DE LA OPERACION SI FALLA ALGUNA OPERACION SE REALIZA UN ROLLBACK
        	$conectado->ejecutarQueryBatch("BEGIN");
       		$conectado->ejecutarQueryBatch($cadena);
			$conectado->ejecutarQueryBatch($cadenaUpdateRInicial);
			$conectado->ejecutarQueryBatch("COMMIT");
        }catch(Exception $e) {
        	$conectado->ejecutarQueryBatch("ROLLBACK");
        	//SI SE PRODUCE UN ERROR EN ALGUNO DE LOS DOS QUERYS REALIZO UN ROLLBACK
			$conectado->cerrar_conexion();

         	if(strpos($e, "uc_tel_cliente")){
         		$resultado = "Error: Tel&eacute;fono ya registrado en la base de datos";
         	}else if (strpos($e, "uc_codigo_cliente")) {
         		$resultado = "Error: C&oacute;digo de cliente ya registrado en la base de datos";
         	}else if (strpos($e, "uc_email_cliente")) {
         		$resultado = "Error: Email del cliente ya registrado en la base de datos";
         	}else {
         		$resultado = "Ocurri&oacute; un error inesperado al realizar la operaci&oacute;n";
         	}
        }
		$conectado->cerrar_conexion();
		return $resultado;
	}
}