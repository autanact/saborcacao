<div id="slides">
  <img src="<?php print(DIR_WS_IMAGENES) ?>banner1.JPG">	
  <img src="<?php print(DIR_WS_IMAGENES) ?>banner2.JPG">
  <img src="<?php print(DIR_WS_IMAGENES) ?>banner3.JPG">
  <!-- 
  <img src="<?php print(DIR_WS_IMAGENES) ?>banner4.JPG">
  -->
  <img src="<?php print(DIR_WS_IMAGENES) ?>banner5.JPG">
  <img src="<?php print(DIR_WS_IMAGENES) ?>banner6.JPG">
  <img src="<?php print(DIR_WS_IMAGENES) ?>banner7.JPG">
  <img src="<?php print(DIR_WS_IMAGENES) ?>banner8.JPG">
  <img src="<?php print(DIR_WS_IMAGENES) ?>banner9.JPG">
</div>

<!-- SlidesJS Required: Link to jQuery -->
<script src="<?php print(DIR_WS_JS) ?>jquery-1.9.1.min.js"></script>
<!-- End SlidesJS Required -->

<!-- SlidesJS Required: Link to jquery.slides.js -->
<script src="<?php print(DIR_WS_JS) ?>jquery.slides.min.js"></script>
<!-- End SlidesJS Required -->
  
<script>
    $(function() {
      $('#slides').slidesjs({
        width: 0,
        height: 0,
		navigation: {
			active: false,
			// [boolean] Generates next and previous buttons.
			// You can set to false and use your own buttons.
			// User defined buttons must have the following:
			// previous button: class="slidesjs-previous slidesjs-navigation"
			// next button: class="slidesjs-next slidesjs-navigation"
			effect: "slide"
			// [string] Can be either "slide" or "fade".
		},
        play: {
          active: false,
          auto: true,
          interval: 4000,
          swap: true
        }, 
		pagination: {
		  active: false,
			// [boolean] Create pagination items.
			// You cannot use your own pagination. Sorry.
		  effect: "slide"
			// [string] Can be either "slide" or "fade".
		}
      });
    });
</script>