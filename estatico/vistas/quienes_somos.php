<div id="quienes_somos_estatico"> 
	<p>
			<b>Sabor Cacao</b> es una empresa vanguardista que nace de un equipo apasionado por el mundo del chocolate, 
			quienes tienen el prop�sito de crear productos con el mejor cacao combinado a los m�s frescos ingredientes,
			 buscando como resultado la perfecci�n en sabor, equilibrio y textura, convirtiendo 
			 cada preparaci�n en un <b>Arte del Sabor.</b>
	</p>
</div> 