<?php
/*
 * Created on 15/06/2009
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

define('HTTP_SERVER', '../../');
define('DIR_WS_PRODUCTOS','../../');
define('DIR_WS_ESTATICO','');
define('DIR_WS_FLASH', '../../flash/');
define('DIR_WS_IMAGENES', '../../imagenes/');
define('DIR_WS_PLANTILLAS', '../../plantillas/');
define('DIR_WS_CSS', '../../css/');
define('DIR_WS_JS', '../../js/');
define('DIR_WS_PDF','../../pdf/');
define('DIR_WS_CLASES','../../clases/');
define('DIR_WS_PRIMERREGISTRO','../../primer_registro/vistas/');
define('DIR_WS_OPERADORAS','../../operadoras/vistas/');
define('DIR_WS_PST','../../pst/vistas/');
define('DIR_WS_COMERCIALIZADORES','../../comercializadores/vistas/');
define('DIR_WS_IOBPAS','../../iobpas/vistas/');
define('DIR_WS_CONALOT','../../conalot/vistas/');
define('DIR_WS_CENTROAPUESTA','../../ca/vistas/');
define('DIR_WS_CONSULTOR','../../consultor/vistas/');
define('DIR_WS_LOGS','../../logs/');
?>