$(function(){
	$(".btn-agregar-producto").off("click");
	$(".btn-agregar-producto").on("click", function(e) {
		$(".exito-detalle-producto").hide();
		var idProducto = $("#txt_idproducto").val();
		var producto = $("#txt_producto").val();
		var unidadProducto = $("#txt_unidad_producto").val();
		//SE CAPTURA TABLA QUE ALMACENA LOS DATOS DEL DETALLE DEL PEDIDO
		var tablaDatos= $("#tblDatos");
		//SE PASA 1 PARA QUE AGREGUE EL ID DE PRODUCTO DE LA VARIABLE DE SESSION
		var url = '../controller/producto_controller.php?page=1';

		$.post( url,
        {   idProducto: idProducto,
			producto: producto,
			unidad: unidadProducto
         },
            function(data){
            //donothing
        });
		
		//SE MODIFICA EL Z-INDEX
		document.getElementById("productos_compra").style.zIndex = "200";
		
		//VARIABLE QUE PERMITE SABER SI LA FUNCION ELIMINARFILA FUE LLAMADA DESDE CODIGO AJAX O DESDE CODIGO PHP
		var baderaPHP=0;
		//SE AGREGA LA LINEA AL FINAL DE LA TABLA A TRAVEZ DE JAVASCRIPT 
		tablaDatos.append("<tr class='modo1' id='"+idProducto+"'><td>"+producto+"</td>" +
						"<td class='modo2'> "+
	        				"<div class='cantidadInput'>" +
	        					"<input value='"+producto+"' id='nombre_producto_"+idProducto+"'  type='hidden' />"+
	        					"<input value='1' type='text' id='cantidad_"+idProducto+"'  maxlength='3' onkeypress='return validarNumero(event)' /></div>"+
	        					"<div class='incrementaDecrementa'>"+
	        					"<a onclick=aumentarCantidad("+idProducto+")><img src='../imagenes/incrementa.png'></a>"+
	        					"<br>"+
	        					"<a onclick=disminuirCantidad("+idProducto+")><img src='../imagenes/decrementa.png'></a>"+
	        					"</div>"+
	        					"<div class='unidades'>&nbsp;"+unidadProducto+"</div>"+
	        			"</td>"+
	        			
	        			"<td>"+
							"<a class='eliminar-producto'  id='eliminar_"+idProducto+"'"+
							"onclick=eliminarFila("+idProducto+",this,"+baderaPHP+")>"+
							"<img src='../imagenes/icono_menos.png'>"+
							"</a>"+
						"</td>"+
	        			
						"</tr>"); 

		//$("#productos_compra").show();
	});
	
	$(".eliminar-producto").off("click");
	$(".eliminar-producto").on("click", function(e) {
		var idProducto = $("#txt_idproducto").val();
		//SE PASA 2 PARA QUE ELIMINE EL ID DE PRODUCTO DE LA VARIABLE DE SESSION
		var url = '../controller/producto_controller.php?page=2';
		
		$.post( url,
        {   idProducto: idProducto
         },
            function(data){
            //donothing
        });
	});
	
	//FUNCION QUE RECARGA LA PAGINA DE LOS PEDIDOS AL HACER CLIC EN EL LISTADO INICIAL
	$(".ver-pedido").off("click");
	$(".ver-pedido").on("click", function(e) {
		var idRegistroInicial = $(this).attr("id");
		
		//SE GUARDA EL ID DE REGISTRO INICIAL EN UN INPUT OCULTO
		document.getElementById('idregistroinicialinput').value = idRegistroInicial; 

		//CAPTURO EL EMAIL
		var email = document.getElementById("email_"+idRegistroInicial).value;
		//CAPTURO EL TELEFONO
		var telefono = document.getElementById("telefono_"+idRegistroInicial).value;
		
		var urlpedidosinicial = 'pedidos/vistas/lista_pedidos_inicial.php?idregistroinicial='+idRegistroInicial;
		var urldatoscliente   = 'pedidos/vistas/datos_cliente.php';

		//A�adimos la imagen de carga en el contenedor
        $('#listapedidosinicial').html('<div><img src="../imagenes/cargando.gif"></div>');
        $('#datoscliente').html('<div><img src="../imagenes/cargando.gif"></div>');
        $('#listapedidosfinal').html('<div><img src="../imagenes/cargando.gif"></div>');
		$("#listapedidosinicial").load(urlpedidosinicial);
		
		var dataString = "email="+email+"&telefono="+telefono+" ";

		$.ajax({
			type: "POST",
			url: urldatoscliente,
			data: dataString,
			success: function(data){
				$("#datoscliente").html(data);
				
				//SI EL ID DEL CLIENTE ES NULO LE PASO EL VALOR 0
				if($('#idcliente').val() == "" || $('#idcliente').val()== null){
					$('#idcliente').val("0");
				}
				var urlpedidosfinal = 'pedidos/vistas/lista_pedidos_final.php?idcliente='+$('#idcliente').val()+'&tipooperacion=3'; //SE ENVIA 3 PARA QUE NO BUSQUE EL HISTORICO
				$("#listapedidosfinal").load(urlpedidosfinal);
			}
		});
	});
	
	//CUANDO SE PRESIONA LA FLECHA PARA AUMENTAR LA CANTIDAD
	$(".actualizar-cantidad").off("click");
	$(".actualizar-cantidad").on("click", function(e) {
		var idProducto    = $(this).attr("id");
		var idtipoenviado = $("#id_tipo_"+idProducto).val();
		var cantidadtotal = 0;
		
		//var cantidad   = $("#cantidad_"+idProducto).val();
		
		var totalRegistros = document.getElementById('tblDatosProductoInicial').rows.length -1;
		//SE RECORRE LA TABLA QUE CONTIENE LA LISTA DE LOS PRODUCTOS
		for (i=0;i <= totalRegistros; i++){
			idProductoTable = document.getElementById('tblDatosProductoInicial').rows[i].id;
			//SI EL ID DEL PRODUCTO NO FUE ENCONTRADO PUEDE QUE SEA UN REGISTRO DEL HEAD DE LA TABLA
			//EN ESE CASO SE PASA A LA SIGUIENTE ITERACION
			if(idProductoTable==""){
				continue;
			}
			
			//SE CAPTURA LA CANTIDAD A PEDIR POR PRODUCTO
			cantidad     = document.getElementById("cantidad_"+idProductoTable).value;
			//SE CAPTURA EL ID DEL TIPO DEL PRODUCTO (UNIDADES, CAJAS, GRAMOS, ETC)
			idtipo       = document.getElementById("id_tipo_"+idProductoTable).value;
			
			//SE VAN A SUMAR LAS CANTIDADES DE UN MISMO TIPO DE PRODUCTO
			//SI EL ID DE TIPO QUE ESTA EN LA TABLA ES IGUL AL MISMO TIPO QUE SE MODIFICO AL MODIFICAR LA CANTIDAD
			if(idtipo==idtipoenviado){
				cantidadtotal+=parseInt(cantidad);  
			}
		}
		
		//SE COLOCA IMAGEN CARGANDO AL DIV
		$("#minprecio_"+idtipoenviado).html('<div><center><img src="../imagenes/cargando.gif"></center></div>');
		$("#maxprecio_"+idtipoenviado).html('<div><center><img src="../imagenes/cargando.gif"></center></div>');
		$("#cantidad_total_"+idtipoenviado).html('<div><center><img src="../imagenes/cargando.gif"></center></div>');
		
		var urlactualizarprecio = '../controller/actualizar_precio_producto.php';
		var dataString = 'idtipo='+idtipoenviado+'&cantidad='+cantidadtotal;
	    
		
		$.ajax({
			type: "POST",
			url: urlactualizarprecio,
			data: dataString,
			success: function(data){
				arreglo_data = data.split("*");
				var minprecio = arreglo_data[0];
				var maxprecio = arreglo_data[1];
				
				//SE SETEAN LOS INPUT HIDDEN CON LOS NUEVOS VALORES
				$("#minprecio_value_"+idtipoenviado).val(minprecio);
				$("#maxprecio_value_"+idtipoenviado).val(maxprecio);
				
				$("#cantidad_total_"+idtipoenviado).html(cantidadtotal);
				$("#minprecio_"+idtipoenviado).html(minprecio);
				$("#maxprecio_"+idtipoenviado).html(maxprecio);
				
			}
		});
	});
	
	//CUANDO SE ESCRIBE UNA CANTIDAD EN EL INPUT DE CANTIDAD
	$( ".actualizar-cantidad-escrita" ).keyup(function() {
		var nombreInputCantidad = $(this).attr("id");
		var cantidad   = $("#"+nombreInputCantidad).val();
		var idProducto = nombreInputCantidad.replace("cantidad_","");

		if(cantidad == "" || cantidad <= 0){
			cantidad = 1;
			$("#"+nombreInputCantidad).val("1");
		}
		
		var idtipoenviado = $("#id_tipo_"+idProducto).val();
		var cantidadtotal = 0;

		var totalRegistros = document.getElementById('tblDatosProductoInicial').rows.length -1;
		//SE RECORRE LA TABLA QUE CONTIENE LA LISTA DE LOS PRODUCTOS
		for (i=0;i <= totalRegistros; i++){
			idProductoTable = document.getElementById('tblDatosProductoInicial').rows[i].id;
			//SI EL ID DEL PRODUCTO NO FUE ENCONTRADO PUEDE QUE SEA UN REGISTRO DEL HEAD DE LA TABLA
			//EN ESE CASO SE PASA A LA SIGUIENTE ITERACION
			if(idProductoTable==""){
				continue;
			}
			
			//SE CAPTURA LA CANTIDAD A PEDIR POR PRODUCTO
			cantidad     = document.getElementById("cantidad_"+idProductoTable).value;
			//SE CAPTURA EL ID DEL TIPO DEL PRODUCTO (UNIDADES, CAJAS, GRAMOS, ETC)
			idtipo       = document.getElementById("id_tipo_"+idProductoTable).value;
			
			//SE VAN A SUMAR LAS CANTIDADES DE UN MISMO TIPO DE PRODUCTO
			//SI EL ID DE TIPO QUE ESTA EN LA TABLA ES IGUL AL MISMO TIPO QUE SE MODIFICO AL MODIFICAR LA CANTIDAD
			if(idtipo==idtipoenviado){
				cantidadtotal+=parseInt(cantidad);  
			}
		}
		
		//SE COLOCA IMAGEN CARGANDO AL DIV
		$("#minprecio_"+idtipoenviado).html('<div><center><img src="../imagenes/cargando.gif"></center></div>');
		$("#maxprecio_"+idtipoenviado).html('<div><center><img src="../imagenes/cargando.gif"></center></div>');
		$("#cantidad_total_"+idtipoenviado).html('<div><center><img src="../imagenes/cargando.gif"></center></div>');
		
		var urlactualizarprecio = '../controller/actualizar_precio_producto.php';
		var dataString = 'idtipo='+idtipoenviado+'&cantidad='+cantidadtotal;
	    
		
		$.ajax({
			type: "POST",
			url: urlactualizarprecio,
			data: dataString,
			success: function(data){
				arreglo_data = data.split("*");
				var minprecio = arreglo_data[0];
				var maxprecio = arreglo_data[1];
				
				//SE SETEAN LOS INPUT HIDDEN CON LOS NUEVOS VALORES
				$("#minprecio_value_"+idtipoenviado).val(minprecio);
				$("#maxprecio_value_"+idtipoenviado).val(maxprecio);
				
				$("#cantidad_total_"+idtipoenviado).html(cantidadtotal);
				$("#minprecio_"+idtipoenviado).html(minprecio);
				$("#maxprecio_"+idtipoenviado).html(maxprecio);
				
			}
		});
	});
	
	//CUANDO SE PRESIONA EL BOTON ACTUALIZAR REGISTRO INICIAL DE LA LISTA INICIAL
	$(".actualizar-registro-inicial").off("click");
	$(".actualizar-registro-inicial").on("click", function(e) {
		//SE COLOCA IMAGEN CARGANDO A LOS REGISTROS INICIAL Y A PEDIDO INICIAL
		$('#listaregistroinicial').html('<div><img src="../imagenes/cargando.gif"></div>');
		var urllistainicial = 'pedidos/vistas/lista_reginicial.php';
		$("#listaregistroinicial").load(urllistainicial);
	});	
	
	//CUANDO SE PRESIONA EL BOTON CERRAR SESSION 
	$(".cerrar-session").off("click");
	$(".cerrar-session").on("click", function(e) {
		//SE COLOCA IMAGEN CARGANDO A LOS REGISTROS INICIAL Y A PEDIDO INICIAL
		$('#listaregistrofinal').html('<div><img src="../imagenes/cargando.gif"></div>');
		$('#listaregistroinicial').html('<div><img src="../imagenes/cargando.gif"></div>');
		$('#datoscliente').html('<div><img src="../imagenes/cargando.gif"></div>');
		$('#listapedidosinicial').html('<div><img src="../imagenes/cargando.gif"></div>');
		
		//EN ESTA PAGINA SE REALIZA EL SESSION DESTROY
		var urlcerrarsession  = '../controller/cerrar_session.php';
		
		//PAGINAS DE LOS DIFERENTES DIVS A REFRESCAR
		var urlpedidosfinal   = 'pedidos/vistas/lista_pedidos_final.php';
		var urlpedidosinicial = 'pedidos/vistas/lista_pedidos_inicial.php';
		var urlregistroinicial= 'pedidos/vistas/lista_pedidos_inicial.php';
		var urldatoscliente   = 'pedidos/vistas/datos_cliente.php';
		var urldatoslogin     = 'pedidos/vistas/datos_login.php';
		var urlpedidoinicial  = 'pedidos/vistas/lista_pedidos_inicial.php';
		
		$.ajax({
			type: "POST",
			url: urlcerrarsession,
			data: '',
			success: function(data){
				//SE ACTUALIZAN TODOS LOS DIVS PARA QUE NO SE MUESTREN
				$("#listaregistroinicial").load(urlregistroinicial);
				$("#datoscliente").load(urldatoscliente);
				$("#listapedidosinicial").load(urlpedidosinicial);
				$("#listapedidosfinal").load(urlpedidosfinal);
				$("#login").css('margin-top', '200px');
				$("#login").load(urldatoslogin);
				$("#login").show();
			}
		});
	});	
	
	//CUANDO SE PRECIONA EL CHECK DE ENTREGADO
	$(".entregado").off("click");
	$(".entregado").on("click", function(e) {
		var idPedido = $(this).attr("id");
		
		if($("#"+idPedido).is(':checked')){
			if(confirm("El pedido sera eliminado del listado. Esta seguro ?") == true){
				var urlactualizarpedido = '../controller/actualizar_pedido.php';		
				var dataString = 'idPedido='+idPedido;
				
				$.ajax({
					type: "POST",
					url: urlactualizarpedido,
					data: dataString,
					success: function(data){
						$('#listapedidosfinal').html('<div><img src="../imagenes/cargando.gif"></div>');
						
						var urlpedidosfinal = 'pedidos/vistas/lista_pedidos_final.php?idcliente='+$('#idcliente').val();
						$("#listapedidosfinal").load(urlpedidosfinal);
					}
				});
			}
		}
	});	
	
	//CUANDO SE PRESIONA EL BOTON DE ELIMINAR PEDIDO 
	$(".eliminar-pedido").off("click");
	$(".eliminar-pedido").on("click", function(e) {
		//SE CAPTURA EL ID DE REGISTRO INICIAL SELECCIONADO PARA ELIMINARLO
		idRegistroInicial = $('#idregistroinicialinput').val();
		var urleliminarpedido = '../controller/eliminar_pedido.php';		
		var dataString = 'idPedido='+idRegistroInicial;
		
		//SE COLOCA IMAGEN CARGANDO A LOS REGISTROS INICIAL Y A PEDIDO INICIAL
		$('#listaregistroinicial').html('<div><img src="../imagenes/cargando.gif"></div>');
		$('#listapedidosinicial').html('<div><img src="../imagenes/cargando.gif"></div>');
		$('#datoscliente').html('<div><img src="../imagenes/cargando.gif"></div>');
		$('#listapedidosfinal').html('<div><img src="../imagenes/cargando.gif"></div>');
		
		$.ajax({
			type: "POST",
			url: urleliminarpedido,
			data: dataString,
			success: function(data){
				var urllistainicial   = 'pedidos/vistas/lista_reginicial.php';
				var urlpedidosinicial = 'pedidos/vistas/lista_pedidos_inicial.php?idregistroinicial=0'; //SE ENVIA 0 PARA QUE NO SE CONSIGA NINGUN REGISTRO
				var urldatoscliente   = 'pedidos/vistas/datos_cliente.php';
				var urlpedidosfinal   = 'pedidos/vistas/lista_pedidos_final.php';
				
				$("#listaregistroinicial").load(urllistainicial);
				$("#listapedidosinicial").load(urlpedidosinicial);
				$("#datoscliente").load(urldatoscliente);
				$("#listapedidosfinal").load(urlpedidosfinal);
				
			}
		});
		
	
	});	
	
	//CUANDO SE PRESIONA EL BOTON BUSCAR CLIENTE
	$(".buscarcliente").off("click");
	$(".buscarcliente").on("click", function(e) {
		//tipooperacion = 1 GUARDAR, tipooperacion=2 ACTUALIZA, tipooperacion=3 BUSCA, tipooperacion=4 BUSCA HISTORICO
		tipooperacion = $('#tipooperacion').val();
		
		/*
		if(tipooperacion == null){
			tipooperacion = 3;
		}
		*/
		
		var codigocliente = $('#codigo').val();
		var email         = $('#email').val();
		var telefono      = $('#telefono').val();
		
		//SE VALIDAN LOS CAMPOS OBLIGATORIOS
		if(codigocliente == "" && email=="" && telefono==""){
			alert("Debe introducir un Email, un telefono o un codigo de cliente");
		} else {
			
			//idcliente se toma en cuenta solo cuando es actualizacion en caso contrario se envia en blanco
			var dataString = 'tipooperacion=3&nombrecliente='+$('#nombrecliente').val()+'&nit='+$('#nit').val()+
							 ' &emailoriginal='+$('#emailoriginal').val()+'&telefono='+$('#telefono').val()+' &direccion='+$('#direccion').val()+'&ciudad='+$('#ciudad').val()+
			                 '&municipio='+$('#municipio').val()+'&estado='+$('#estado').val()+'&pais='+$('#pais').val()+
			                 '&email='+$('#email').val()+'&codigo='+$('#codigo').val()+'&tipopersona='+$('#tipopersona').val();
	        
			//SE AGREGA IMAGEN CARGANDO AL DIV
			$('#datoscliente').html('<div><img src="../imagenes/cargando.gif"></div>');
			//SE AGREGA IMAGEN CARGANDO AL DIV
			$('#listapedidosfinal').html('<div><img src="../imagenes/cargando.gif"></div>');	
			//SE AGREGA IMAGEN CARGANDO AL DIV
			$('#listapedidosinicial').html('<div><img src="../imagenes/cargando.gif"></div>');	
			
			var urldatoscliente   = 'pedidos/vistas/datos_cliente.php';

			$.ajax({
	            type: "POST",
	            url: urldatoscliente,
	            data: dataString,
	            success: function(data){
					$('#datoscliente').html(data);
					//SE LE ENVIA 0 PARA QUE NO ENCUENTRE NINGUN REGISTRO
					var urlpedidosinicial = 'pedidos/vistas/lista_pedidos_inicial.php?idregistroinicial=0';
					//SE ENVIA EL ID DEL CLIENTE A LA LISTA DE PEDIDO FINAL
					var urlpedidosfinal = 'pedidos/vistas/lista_pedidos_final.php?idcliente='+$('#idcliente').val()+'&tipooperacion='+tipooperacion;
					$("#listapedidosfinal").load(urlpedidosfinal);
					//SE RECARGA EL DIV DE PEDIDO INICIAL
					$("#listapedidosinicial").load(urlpedidosinicial);
					alertify.success("Operaci&oacute;n Realizada con &Eacute;xito");
	            }
	        });
		}
	});
	
	//SE PRESIONA EL BOTON LIMPIAR DEL FORMULARIO
	$(".limpiar").off("click");
	$(".limpiar").on("click", function(e) {
		$('#idcliente').val("");
		$('#nombrecliente').val("");
		$('#nit').val("");
		$('#emailoriginal').val("");
		$('#telefono').val("");
		$('#direccion').val("");
		$('#ciudad').val("");
		$('#municipio').val("");
		$('#estado').val("");
		$('#pais').val("");
		$('#email').val("");
		$('#codigo').val("");
	});
	
	//VALIDACION DE FORMULARIO DE REGISTRO INICIAL
	$(document).ready(function() {
		// validate the comment form when it is submitted
		/*
		jQuery.validator.addMethod("telefono", function(value, element) {
		  return this.optional(element) || /^\+[0-9]{10}$/.test(value);
		}, "Por favor ingresa un tel&eacute;fono v&aacute;lido. ");
		*/
		jQuery.validator.addMethod("telefono", function(value, element) {
			var codigo = $("#txt_codigo").val();
			//SI EL CODIGO TIENE ALGUN VALOR NO VALIDO EL TELEFONO
			if(codigo!=""){
				return true;
			}else {
				return this.optional(element) || /^\+[0-9]{10,13}$/.test(value);
			}
		}, "Por favor ingresa un tel&eacute;fono v&aacute;lido. ");
		
		// validate signup form on keyup and submit
		$("#registroInicial").validate({
			rules: {
				txt_telefono: {
					required: true, 
					email: false, 
					telefono: true
				},	
				txt_email: {
					required: true,
					email: true
				},
				txt_codigo: {
					required: false
				}
			},
			messages: {
				txt_telefono: "Por favor ingresa un tel&eacute;fono v&aacute;lido.",
				txt_email: "Por favor ingresa un email v&aacute;lido.",
				txt_codigo: "Por favor ingresa un c&oacute;digo v&aacute;lido."
			},
	        submitHandler: function(form){
				//SE DESHABILITA EL BOTON GENERAR PEDIDO
				document.getElementById("generarpedidoboton").disabled = true;
				var i=1;
				var productos="";
				var totalRegistros = document.getElementById('tblDatos').rows.length -1;
				//SE RECORRE LA TABLA QUE CONTIENE LA LISTA DE LOS PRODUCTOS
				for (i=1;i <= totalRegistros; i++){
					idProductoTable = document.getElementById('tblDatos').rows[i].id;
					cantidad = document.getElementById("cantidad_"+idProductoTable).value;
					nombreProducto = document.getElementById("nombre_producto_"+idProductoTable).value;
					
					//SE ARMA LA VARIABLE QUE ALMACENARA LOS PRODUCTOS
					
					//SI ES EL ULTIMO REGISTRO NO AGREGO EL * AL FINAL
					if(i==totalRegistros){
						productos+=  idProductoTable+","+cantidad+","+nombreProducto;
					} else {
						productos+=  idProductoTable+","+cantidad+","+nombreProducto+"*";
					}
				}
				
				var dataString = 'txt_telefono='+$('#txt_telefono').val()+'&txt_email='+$('#txt_email').val()+'&txt_codigo='+$('#txt_codigo').val()+'&productos='+productos;
	            $.ajax({
	                type: "POST",
	                url:"../controller/procesar_formulario.php",
	                data: dataString,
	                success: function(data){
	            		var urldetalleproducto = '../productos/vistas/productos_detalle.php';
	            		//alertify.success("Su pedido ha sido generado exitosamente");
	            	    var tabla;
	            	    $(".exito-detalle-producto").html(data);
	            	    $(".exito-detalle-producto").show();
	            		$(".detalle-producto").load(urldetalleproducto);
	                    resetearChecks();
	                }
	            });
	        }
		});
	});
	
	//FUNCION PARA FORMALIZAR EL PEDIDO
	$(document).ready(function() {
		// validate signup form on keyup and submit
		$("#generarpedido").validate({
			rules: {
					fechaentrega: {
						required: true,
						email: false
					}, 
					precio_pedido_1: {
						required: true,
						email: false
					},
					precio_pedido_2: {
						required: true,
						email: false
					},
					precio_pedido_3: {
						required: true,
						email: false
					}
			},
			messages: {
				fechaentrega: "La fecha de entrega es requerida",
				precio_pedido_1: "El precio es requerido",
				precio_pedido_2: "El precio es requerido",
				precio_pedido_3: "El precio es requerido"
			},
	        submitHandler: function(form){
				var idcliente = $('#idcliente').val();
				//EL CLIENTE NO ESTA REGISTRADO
				if(idcliente=="" || idcliente==null || idcliente=="0" || idcliente==0){
					alertify.error("Debes registrar al cliente para poder generar un pedido");
				}else {
					var i=0;
					var productos="";
					var montoTotal=0;
					var bandera=true;
					var totalRegistros = document.getElementById('tblDatosProductoInicial').rows.length -1;
					//SE RECORRE LA TABLA QUE CONTIENE LA LISTA DE LOS PRODUCTOS
					for (i=0;i <= totalRegistros; i++){
						idProductoTable = document.getElementById('tblDatosProductoInicial').rows[i].id;
						//SI EL ID DEL PRODUCTO NO FUE ENCONTRADO PUEDE QUE SEA UN REGISTRO DEL HEAD DE LA TABLA
						//EN ESE CASO SE PASA A LA SIGUIENTE ITERACION
						if(idProductoTable==""){
							continue;
						}
						
						//SE CAPTURA LA CANTIDAD A PEDIR POR PRODUCTO
						cantidad          = document.getElementById("cantidad_"+idProductoTable).value;
						//SE CAPTURA LA CANTIDAD ORIGINAL QUE TENIA O TIENE EL PEDIDO
						cantidadoriginal = document.getElementById("cantidad_original_"+idProductoTable).value;
			
						//SE CAPTURA EL ID DEL TIPO DEL PRODUCTO (UNIDADES, CAJAS, GRAMOS, ETC)
						idtipo       = document.getElementById("id_tipo_"+idProductoTable).value;
						//SE CAPTURA EL PRECIO QUE SE INTRODUJO PARA ESE TIPO DE PRODUCTO
						precio         = document.getElementById("precio_pedido_"+idtipo).value;
						//EL PRECIO ES BLANCO NO SE INTROJO NINGUN VALOR
						
						if(precio=="" || precio==null){
							bandera=false;
							break;
						}
						
						maxpreciovalueori = document.getElementById("maxprecio_value_"+idtipo).value;
						minpreciovalueori = document.getElementById("minprecio_value_"+idtipo).value;
						
						//SE REEMPLAZA EL PUNTO POR NADA
						maxpreciovalue = maxpreciovalueori.replace(".","");
						minpreciovalue = minpreciovalueori.replace(".","");
						precio         = precio.replace(".","");
						//SE REEMPLAZA LA COMA POR UN PUNTO
						maxpreciovalue = maxpreciovalue.replace(",",".");
						minpreciovalue = minpreciovalue.replace(",",".");
						precio         = precio.replace(",",".");
						precio         = parseFloat(precio);
						
						/*
						 * SE QUITA LA VALIDA PORQUE NO ES REQUERIDA POR EL USUARIO
						if(precio < minpreciovalue || precio > maxpreciovalue){
							//alert("El precio debe estar entre "+minpreciovalueori+" y "+maxpreciovalueori);
							alertify.error("El precio debe estar entre "+minpreciovalueori+" y "+maxpreciovalueori);
							bandera=false;
							break;
						}
						*/
						
						//SE ARMA LA VARIABLE QUE ALMACENARA LOS PRODUCTOS
						//SI ES EL ULTIMO REGISTRO NO AGREGO EL * AL FINAL
						if(i==totalRegistros){
							productos+=  idProductoTable+","+cantidad+","+precio+","+cantidadoriginal;
						} else {
							productos+=  idProductoTable+","+cantidad+","+precio+","+cantidadoriginal+"*";
						}
						//SE VA ACUMULANDO EL MONTO TOTAL
						montoTotal+=parseFloat(precio);
					}
					
					
					if(bandera){
						//SE COLOCA IMAGEN .GIF CARGANDO
						$('#listapedidosfinal').html('<div><img src="../imagenes/cargando.gif"></div>');
						var dataString = 'montototal='+montoTotal+'&idregistroinicialinput='+$('#idregistroinicialinput').val()+'&fechaentrega='+$('#fechaentrega').val()+'&idcliente='+$('#idcliente').val()+'&productos='+productos;
			            $.ajax({
			                type: "POST",
			                url:"../controller/procesar_pedido.php",
			                data: dataString,
			                success: function(data){
			            		//SE COLOCA IMAGEN CARGANDO A LOS REGISTROS INICIAL Y A PEDIDO INICIAL
			            		$('#listaregistroinicial').html('<div><img src="../imagenes/cargando.gif"></div>');
			            		$('#listapedidosinicial').html('<div><img src="../imagenes/cargando.gif"></div>');
			            		
								//SE ENVIA EL ID DEL CLIENTE
								var urlpedidosfinal = 'pedidos/vistas/lista_pedidos_final.php?idcliente='+$('#idcliente').val();
								var urllistainicial = 'pedidos/vistas/lista_reginicial.php';
								//SE LE ENVIA 0 PARA QUE NO ENCUENTRE NINGUN REGISTRO
								var urlpedidosinicial = 'pedidos/vistas/lista_pedidos_inicial.php?idregistroinicial=0';
								
			            		//document.getElementById('tblDatosProductoInicial').style.width = "650px";
			            		//SE RECARGA EL DIV DE LISTA DE PEDIDOS FINALES
			            		$("#listapedidosfinal").load(urlpedidosfinal);
								//SE RECARGA EL DIV DE LA LISTA INICIAL
								$("#listaregistroinicial").load(urllistainicial);
								$("#listapedidosinicial").load(urlpedidosinicial);
			            		alertify.success("Su pedido ha sido generado exitosamente");
			                }
			            });
					}
				}
	        }
		});
	});
	

	//FUNCION PARA VALIDAR EL FORMULARIO DE REGISTRO
	$(document).ready(function() {
		jQuery.validator.addMethod("telefono", function(value, element) {
			var codigo = $("#txt_codigo").val();
			//SI EL CODIGO TIENE ALGUN VALOR NO VALIDO EL TELEFONO
			if(codigo!=""){
				return true;
			}else {
				return this.optional(element) || /^\+[0-9]{10,13}$/.test(value);
			}
		}, "Por favor ingresa un tel&eacute;fono v&aacute;lido. ");
		
		// validate signup form on keyup and submit
		$("#guardarDatosCliente").validate({
			rules: {
				nombrecliente: {
					required: true,
					email: false
				},
				nit: {
					required: false,
					email: false
				},
				telefono: {
					required: true,
					email: false,
					telefono: true
				},
				direccion: {
					required: true,
					email: false
				},
				ciudad: {
					required: true,
					email: false
				},
				municipio: {
					required: true,
					email: false
				},
				estado: {
					required: true,
					email: false
				},
				pais: {
					required: true,
					email: false
				},
				email: {
					required: true,
					email: true
				},
				codigo: {
					required: false,
					email: false
				}
			},
			messages: {
				nombrecliente: "Por favor ingresa un nombre.",
				telefono: "Por favor ingresa un email v&aacute;lido.",
				direccion: "Por favor ingresa un asunto.",
				ciudad: "Por favor ingresa un mensaje.",
				municipio: "Por favor ingresa un mensaje.",
				estado: "Por favor ingresa un mensaje.",
				pais: "Por favor ingresa un mensaje.",
				email: "Por favor ingresa un mensaje."
			},
	        submitHandler: function(form){ 
				//tipooperacion = 1 GUARDAR, tipooperacion=2 ACTUALIZA
				//idcliente se toma en cuenta solo cuando es actualizacion en caso contrario se envia en blanco
				var email = $('#email').val();
				
				var dataString = 'idcliente='+$('#idcliente').val()+'&tipooperacion='+$('#tipooperacion').val()+'&nombrecliente='+$('#nombrecliente').val()+'&nit='+$('#nit').val()+
								 ' &emailoriginal='+$('#emailoriginal').val()+'&telefono='+$('#telefono').val()+' &direccion='+$('#direccion').val()+'&ciudad='+$('#ciudad').val()+
				                 '&municipio='+$('#municipio').val()+'&estado='+$('#estado').val()+'&pais='+$('#pais').val()+
				                 '&email='+$('#email').val()+'&codigo='+$('#codigo').val()+'&tipopersona='+$('#tipopersona').val();
					
				//SE AGREGA IMAGEN CARGANDO AL DIV
				$('#datoscliente').html('<div><img src="../imagenes/cargando.gif"></div>');
				//SE COLOCA IMAGEN CARGANDO A LOS REGISTROS INICIAL Y A PEDIDO INICIAL
				$('#listaregistroinicial').html('<div><img src="../imagenes/cargando.gif"></div>');
				
				$.ajax({
	                type: "POST",
	                url:"../controller/procesar_datos_cliente.php",
	                data: dataString,
	                success: function(data){
						if(data == "true"){
							alertify.success("Operaci&oacute;n Realizada con &Eacute;xito");
						}else {
							alertify.error(data);
						}
						
						var urllistainicial = 'pedidos/vistas/lista_reginicial.php';
						$("#listaregistroinicial").load(urllistainicial);
						var urldatoscliente   = 'pedidos/vistas/datos_cliente.php?email='+email;
						$("#datoscliente").load(urldatoscliente);
	                }
	            });
	        }
		});
	});
	
	//VALIDACION DE LOGEO. ESTA FUNCION SE LLAMA AL MOMENTO DE PRESIONAR EL BOTON INGRESAR
	//DEL FORMULARIO INICIAL DE LOGUEO. SI EL USUARIO ES VALIDO REFRESCA TODOS LOS DIVS
	$(document).ready(function() {
		// validate signup form on keyup and submit
		$("#formulariologin").validate({
			rules: {
				usuario: {
					required: true
				},	
				clave: {
					required: true
				}
			},
			messages: {
				usuario: "Por favor ingresa un usuario.",
				clave: "Por favor ingresa una clave.",
			},
	        submitHandler: function(form){
				var dataString = 'usuario='+$('#usuario').val()+'&clave='+$('#clave').val();
				var urldatoslogin = "pedidos/vistas/datos_login.php"
				var procesarlogin = "../controller/procesar_login.php"
					
				/*
				//SE COLOCA IMAGEN CARGANDO A LOS REGISTROS INICIAL Y A PEDIDO INICIAL
				$('#listaregistrofinal').html('<div><img src="../imagenes/cargando.gif"></div>');
				$('#listaregistroinicial').html('<div><img src="../imagenes/cargando.gif"></div>');
				$('#datoscliente').html('<div><img src="../imagenes/cargando.gif"></div>');
				$('#listapedidosinicial').html('<div><img src="../imagenes/cargando.gif"></div>');
				*/
					
				//PAGINAS DE LOS DIFERENTES DIVS A REFRESCAR
				var urlpedidosfinal   = 'pedidos/vistas/lista_pedidos_final.php';
				var urlpedidosinicial = 'pedidos/vistas/lista_pedidos_inicial.php';
				var urldatoscliente   = 'pedidos/vistas/datos_cliente.php';
				var urldatoslogin     = 'pedidos/vistas/datos_login.php';
				var urlpedidoinicial  = 'pedidos/vistas/lista_pedidos_inicial.php';
				var urlregistroinicial= 'pedidos/vistas/lista_reginicial.php';

				//SE COLOCA IMAGEN CARGANDO AL LOGIN
				$('#login').html('<div><img src="../imagenes/cargando.gif"></div>');
				$.ajax({
	                type: "POST",
	                url: procesarlogin,
	                data: dataString,
	                success: function(data){
						//NO HUBO EXITO AL MOMENTO DE HACER EL LOGIN DEL USUARIO
						if(data=="false"){
							$("#login").load(urldatoslogin);
						}else if (data=="true") {
							$("#login").css('margin-top', '0px');
							//oculto el div de login
							$("#login").hide();
							$('#listaregistroinicial').html('<div><img src="../imagenes/cargando.gif"></div>');
							$('#datoscliente').html('<div><img src="../imagenes/cargando.gif"></div>');
							
							$("#listaregistroinicial").load(urlregistroinicial);
							$("#datoscliente").load(urldatoscliente);
							
							$("#listapedidosinicial").load(urlpedidoinicial);
							$("#listaregistrofinal").load(urlpedidosfinal);
						}
	                }
	            });
	            
	        }
		});
	});
	
	$(document).ready(function(){
	    //SOLO DOS DECIMALES CON EL SEPARADOR DE LA COMA, Y NO PERMITIR VALORES NEGATIVOS
	    $(".decimal").numeric({ decimal : "," , decimalPlaces : 2,  negative : false});
	});	
});