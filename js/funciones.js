function setearValorProducto(producto,idProducto,cantidad, unidadProducto){
	var nombreBoton = "agregar_"+idProducto;
	document.getElementById("txt_idproducto").value= idProducto;
	document.getElementById("txt_cantidad").value= cantidad;
	document.getElementById("txt_producto").value= producto;
	document.getElementById("txt_unidad_producto").value= unidadProducto;
	//document.getElementById("txt_imagen").value= idImagen;
	//OCULTO EL DIV DE AGREGAR
	document.getElementById("icono_agregar_"+idProducto).style.visibility = "hidden";
	//MUESTRO EL DIV DE ELIMINAR
	document.getElementById("icono_eliminar_"+idProducto).style.visibility = "visible";
	//MUESTRO EL DIV DE CHECK
	document.getElementById("icono_check_"+idProducto).style.visibility = "visible";
	//MUESTRO EL DIV DE LA TABLA DE DATOS
	document.getElementById("tblDatos").style.visibility = "visible";
    //MUESTRO EL DIV DE GENERAR PEDIDO
	document.getElementById("generar_pedido").style.visibility = "visible";
	//OCULTO EL DIV DE FLECHA PEDIDO
	document.getElementById("mensajePedido").style.visibility = "hidden";
	document.getElementById("mensajePedido").style.height = "0px";
}

function eliminarFila(idProducto,r,banderaPHP){
	//banderaPHP ESTE PARAMETRO PERMITE SABER SI LA FUNCION FUE LLAMADA DESDE AJAX O DESDE PHP
	var i=0;
	var idFilaEliminar=-1;
	var totalRegistros = document.getElementById('tblDatos').rows.length -1;
	//CAPTURO EL ID DEL PRODUCTO DE LA TABLA DE PRODUCTOS Y LO COMPARADO CON EL ID QUE VIENE POR PARAMETROS
	//SE RECORRE LA TABLA
	for (i=1;i <= totalRegistros; i++){
		idProductoTable = document.getElementById('tblDatos').rows[i].id;
		//SI EL ID PASADO POR PARAMETRO ES IGUAL AL ID DEL PRODUCTO ENTONCES
		//ROMPO EL CICLO Y VOY A ELIMINAR DE LA TABLA LA LINEA QUE TENGA EL VALOR DE
		//LA VARIABLE idFilaEliminar
		if(idProductoTable == idProducto){
			idFilaEliminar = i;
		}
	} 

	if(idFilaEliminar!= -1){
		//SE ELIMINA LA FILA
		document.getElementById("tblDatos").deleteRow(idFilaEliminar);
		
		//SI NO QUEDAN MAS REGISTRO EN LA TABLA OCULTO EL DIV DE GENERAR PEDIDO Y EL DIV DE LA TABLA
		if(i==2){
			//OCULTO EL DIV DE LA TABLA DE DATOS
			document.getElementById("tblDatos").style.visibility = "hidden";
			//OCULTO EL DIV DE GENERAR PEDIDO
			document.getElementById("generar_pedido").style.visibility = "hidden";
			//MUESTRO EL DIV DE FLECHA PEDIDO
			document.getElementById("mensajePedido").style.visibility = "visible";
			//SE MODIFICA EL Z-INDEX
			document.getElementById("productos_compra").style.zIndex = "-1";
		}
	}
	
	//SE ACTUALIZA EL ID DEL PRODUCTO EN UN CAMPO DE TEXTO
	document.getElementById("txt_idproducto").value= idProducto;
	//MUESTRO EL DIV DE AGREGAR
	document.getElementById("icono_agregar_"+idProducto).style.visibility = "visible";
	//OCULTO EL DIV DE ELIMINAR
	document.getElementById("icono_eliminar_"+idProducto).style.visibility = "hidden";
	//OCULTO EL DIV DE CHECK
	document.getElementById("icono_check_"+idProducto).style.visibility = "hidden";
	
	//SI LA FUNCION FUE LLAMADA DESDE AJAX SE SIMULA UN CLICK
	if(banderaPHP==0){
		//SE SIMULA UN CLIC PARA QUE LLAME A FUNCION DE AJAX
		document.getElementById('eliminar_'+idProducto).click();
	}
}

//ESTA FUNCION CONTROLA CUAL LINK SE DEBE OCULTAR O MOSTRAR EN LA PAGINA
//TAMBIEN CONTROLA CUAL LINK SE DEBE MARCAR COMO ACTIVO Y CUAL NO
function mostrarOcultarElementos(pagina){
	//alert("Pagina es "+pagina);
	switch (pagina) {
		 //SE HIZO CLIC EN EL LINK INICIO 	
	   	case "inicio":
	   		//SE MUESTRAN EL DIVS ACTIVO Y SE OCULTAN LOS INACTIVOS
	   		mostrarOcultarDivs(pagina);
	   		
	   		//SE CAMBIA EL MENU A POSICION NORMAL
	   		cambiarMenuFlotante(pagina);
      	break 
		//SE HIZO CLIC EN EL LINK DE PRODUCTOS
	   	case "productos": 
	   		//SE MUESTRAN EL DIVS ACTIVO Y SE OCULTAN LOS INACTIVOS
	   		mostrarOcultarDivs(pagina);
	   		
	   		//SE CAMBIA EL MENU A FLOTANTE
	   		cambiarMenuFlotante(pagina);
	    break 
	   	default: 
	      	alert("Pagina no encontrada");
	}
}

//ESTA FUNCION SE ENCARGA DE OCULTAR Y MOSTRAR LOS DIVS DEPENDIENDO DEL LINK QUE ACTIVE EN LA PAGINA
function mostrarOcultarDivs(pagina){
	switch (pagina) {
		 //SE HIZO CLIC EN EL LINK INICIO 	
	  	case "inicio":
	  		//OCULTO LOS DIV DE PRODUCTOS IMAGENES Y PRODUCTOS COMPRAS
	  		document.getElementById("productos_imagenes").style.visibility = "hidden";
	  		document.getElementById("productos_imagenes").style.height = "0px";
	  		document.getElementById("productos_compra").style.visibility = "hidden";
	  		document.getElementById("productos_compra").style.height = "0px";
	  		//SE MODIFICA LA POSICION DEL MENU
	  		document.getElementById("menu").style.top = "80%";
	  		//MUESTRO EL DIV DE BANNER
	  		document.getElementById("banner").style.visibility = "visible";
	  		document.getElementById("banner").style.height = "100%";
	 	break 
		//SE HIZO CLIC EN EL LINK DE PRODUCTOS
	  	case "productos": 
	  		//SE MODIFICA LA POSICION DEL MENU
	  		document.getElementById("menu").style.top = "0%";
	  		//OCULTO EL DIV DE BANNER
	  		document.getElementById("banner").style.visibility = "hidden";
	  		document.getElementById("banner").style.height = "0px";
	  		//MUESTRO LOS DIV DE PRODUCTOS IMAGENES Y PRODUCTOS COMPRAS
	  		document.getElementById("productos_imagenes").style.visibility = "visible";
	  		document.getElementById("productos_imagenes").style.height = "100%";
	  		document.getElementById("productos_compra").style.visibility = "visible";
	  		document.getElementById("productos_compra").style.height = "500px";
	  		//SE MUESTRAN LOS CHECKS SI HACE FALTA
	  		//mostrarChecks();
	   break 
	  	default: 
	     	alert("Pagina no encontrada");
	}
}

//SE ENCARGA DE COLOCAR EL MENU FLOTANTE
function cambiarMenuFlotante(pagina) {
	var positionOriginal = "relative";
	var positionNueva = "fixed";
	var paddingOriginal = "50px 0px 0px 0px";
	var paddingNuevo    = "0px 0px 0px 0px";

	switch (pagina) {
		 //SE HIZO CLIC EN EL LINK INICIO 	
	  	case "inicio":
	  		document.getElementById("menu").style.position   = positionOriginal;
	  		document.getElementById("menu").style.padding    = paddingOriginal;
	  		
	  		document.getElementById("productos").style.textDecoration = 'none';
	  		document.getElementById("inicio").style.textDecoration = 'underline';
	 	break 
		//SE HIZO CLIC EN EL LINK DE PRODUCTOS
	  	case "productos": 
	  		document.getElementById("inicio").style.textDecoration = 'none';
	  		document.getElementById("productos").style.textDecoration = 'underline';
	  		//SE COLOCA LA POSITION NUEVA FIXED PARA HACER EL MARCO FLOTANTE
	  		document.getElementById("menu").style.position   = positionNueva;
	  		//SE COLOCA EL PADDING DE 0
	  		document.getElementById("menu").style.padding    = paddingNuevo;
	   break 
	  	default: 
	     	alert("Pagina no encontrada");
    }
} 

//FUNCION QUE AUMENTA LA CANTIDAD EN EL CARRITO DE COMPRAS
function aumentarCantidad(idProducto) {
	var cantidadOriginal;
	cantidadOriginal = Number(document.getElementById("cantidad_"+idProducto).value);
	document.getElementById("cantidad_"+idProducto).value = cantidadOriginal+1;
}

//FUNCION QUE DISMINUYE LA CANTIDAD EN EL CARRITO DE COMPRAS
function disminuirCantidad(idProducto) {
	var cantidadOriginal;
	cantidadOriginal = Number(document.getElementById("cantidad_"+idProducto).value);
	//SI LA CANTIDAD ORIGINAL ES MAYOR A UNO LE RESTO UNO Y SE LO COLOCO AL CAMPO
	//EN CASO CONTRARIO NO HAGO NADA PARA QUE NO QUEDEN VALORES NAGATIVOS EN LAS CANTIDADES
	if(cantidadOriginal > 1){
		document.getElementById("cantidad_"+idProducto).value = cantidadOriginal-1;
	}
}

//VALIDA QUE SOLO PUEDAS INTRODUCIR NUMEROS
function validarNumero(e) {
	tecla = (document.all) ? e.keyCode : e.which; // 2
    if (tecla==8) return true; 
    patron = /\d/; // SOLO SE PERMITEN NUMEROS
    te = String.fromCharCode(tecla);
    return patron.test(te); 
}

function eliminarSession() {
	alert("eliminar session");
}

/*
function mostrarChecks() {
	//ACTIVAN LOS CHECKS SI ES NECESARIO
	for (i=0;i<50;i++){
		iconoAgregar = document.getElementById("icono_agregar_"+i);
		//alert("llegue");
		//SI EL DIV EXISTE
		if(iconoAgregar){
			//SI EL ICONO AGREGAR SE ENCUENTRA OCULTO ENTONCES DEBO ACTIVAR EL CHECK ELIMINAR Y CHECK
			if(document.getElementById("icono_agregar_"+i).style.visibility == "hidden"){
				document.getElementById("icono_eliminar_"+i).style.visibility = "visible";
				document.getElementById("icono_check_"+i).style.visibility = "visible";
			}
			
		}
	}
}
*/

//SE DESACTIVAN TODOS LOS CHECKS EXISTENTES
function oculstarChecks() {
	//SE ELIMINAN TODOS LOS CHECKS Y ELIMINAR IMAGENES
	for (i=0;i<50;i++){
		iconoCheck = document.getElementById("icono_check_"+i);
		//alert("llegue");
		//SI EL DIV EXISTE
		if(iconoCheck){
			document.getElementById("icono_check_"+i).style.visibility = "hidden";
			document.getElementById("icono_eliminar_"+i).style.visibility = "hidden";
		}
	}
}

//SE OCULTAN LOS CHECKS Y SE MUESTRAN 
function resetearChecks() {
	//SE ELIMINAN TODOS LOS CHECKS Y ELIMINAR IMAGENES
	for (i=0;i<50;i++){
		iconoCheck = document.getElementById("icono_check_"+i);
		//alert("llegue");
		//SI EL DIV EXISTE
		if(iconoCheck){
			document.getElementById("icono_check_"+i).style.visibility = "hidden";
			document.getElementById("icono_eliminar_"+i).style.visibility = "hidden";
			document.getElementById("icono_agregar_"+i).style.visibility = "visible";
		}
	}
}

//CAMBIA EL COLOR DE LINEA DE UNA CELDA
function cambiarColorCelda(celda) {
	var colorOriginal="#B1B0B3";
	var colorNuevo="yellow";
	var totalRegistros = document.getElementById('tblRegistroInicial').rows.length -1;
	//SE RECORRE LA TABLA
	for (i=1;i <= totalRegistros; i++){
		document.getElementById('tblRegistroInicial').rows[i].style.backgroundColor=colorOriginal;
	} 
	celda.style.backgroundColor=colorNuevo;
}

//LE COLOCA A TODAS LAS CELDAS EL COLOR ORIGINAL
function resetearColorCeldas() {
	var colorOriginal="#B1B0B3";
	var totalRegistros = document.getElementById('tblRegistroInicial').rows.length -1;
	
	//SE RECORRE LA TABLA
	for (i=1;i <= totalRegistros; i++){
		document.getElementById('tblRegistroInicial').rows[i].style.backgroundColor=colorOriginal;
	} 
}

//ACTUALIZA EL TIPO DE OPERACION
function actualizarTipoOperacion(tipoOperacion) {
	//TIPO DE OPERACION PARA SABER SI LA BUSQUEDA DEL CLIENTE ES HISTORICA O ES NORMAL
	document.getElementById('tipooperacion').value = tipoOperacion;
}




